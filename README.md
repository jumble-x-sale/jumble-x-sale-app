# Jumble-X-Sale App

An app that offers a jumble sale for individuals and professionals and acting as secret santa between individuals.

## Android Studio on Linux

### Enable update

```sh
sudo chmod -R ugo+w /opt/android-studio
```

### Run or debug with Android-x86

To prepare for network connection execute the following command
after adding "~/Android/Sdk/platform-tools" to your path:
```sh
adb tcpip 5555
adb connect 192.168.178.71
adb devices
```

```sh
adb tcpip 5555
adb connect 192.168.178.24
adb devices
```

## Tasks

### Done

* REST-API binding (updated and in sync with the backend)
* Background login once the credentials are available
* Screens (Fragment, ViewModel, Adapter?):
    * Account
        * Login
        * Register
        * Account (simply with the option to log out)
    * Sale
        * Purchase (sale offers from other users)
        * Purchased (completed sales from other users)
        * Sell (sale offers from this user)
        * Sold (completed sales from this user)
    * Secret Santa
        * Takeable (secret santa offers from other users)
        * Givable (secret santa offers from this user)
        * Given & Taken (completed secret santas)
    * Message
        * User Messages (messages directly to this user)
        * Sale Messages (message related to sale offers from this user)

### Todo

* Screens (Fragment, ViewModel, Adapter?):
    * Message
        * Completed Sale Messages (message related to completed sales from this user)
* Dynamic change of Light/Dark Themes
* Push messages
* Many small things...
