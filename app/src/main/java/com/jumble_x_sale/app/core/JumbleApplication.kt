@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.core

import android.app.Application
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.BuildConfig
import com.jumble_x_sale.app.view.MainActivity
import com.jumble_x_sale.app.MobileNavigationDirections
import com.jumble_x_sale.app.data.model.Credentials
import com.jumble_x_sale.app.data.model.Session
import com.jumble_x_sale.app.data.model.util.DualValueFilter
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.model.util.MultiValueFilter
import com.jumble_x_sale.app.data.model.util.SingleValueFilter
import com.jumble_x_sale.app.data.network.JumbleApi
import com.jumble_x_sale.app.data.network.adapter.BigDecimalJsonAdapter
import com.jumble_x_sale.app.data.network.adapter.GenericCollectionAdapterFactory
import com.jumble_x_sale.app.data.network.adapter.InstantJsonAdapter
import com.jumble_x_sale.app.data.network.adapter.UUIDJsonAdapter
import com.jumble_x_sale.app.data.repo.*
import com.jumble_x_sale.app.util.toUuid
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import java.util.UUID

class JumbleApplication : Application() {

    private lateinit var preferences: SharedPreferences
    var mainActivity: MainActivity? = null

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Log.e(TAG, "onCreate called")
        }

        initEncryptedSharedPreferences()
    }

    private fun initEncryptedSharedPreferences() {
        val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
        val preferencesName = getString(R.string.preference_file_key)

        preferences = EncryptedSharedPreferences.create(
            preferencesName,
            masterKeyAlias,
            applicationContext,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM)
    }

    val userId: UUID?; get() = preferences.getString(KEY_USER_ID, null).toUuid()
    val email: String?; get() = preferences.getString(KEY_EMAIL, null)
    val password: String?; get() = preferences.getString(KEY_PASSWORD, null)
    val accessToken: String?; get() = preferences.getString(KEY_ACCESS_TOKEN, null)

    fun setSession(session: Session? = null, email: String? = null, password: String? = null) {
        preferences.edit()
            .setUserId(session?.userId)
            .setAccessToken(session?.accessToken)
            .setEmail(email)
            .setPassword(password)
            .apply()
    }

    fun setSession(session: Session?) {
        preferences.edit()
            .setUserId(session?.userId)
            .setAccessToken(session?.accessToken)
            .apply()
    }

    private fun Editor.setUserId(userId: UUID?): Editor { return putString(KEY_USER_ID, userId?.toString()) }
    private fun Editor.setEmail(email: String?): Editor { return putString(KEY_EMAIL, email) }
    private fun Editor.setPassword(password: String?): Editor { return putString(KEY_PASSWORD, password) }
    private fun Editor.setAccessToken(accessToken: String?): Editor { return putString(KEY_ACCESS_TOKEN, accessToken) }

    /** Is the app ready for a login? */
    val isReadyForLogin; get() = email != null && password != null

    /** The credentials for a login. */
    val credentials; get() = Credentials(email!!, password!!)

    /** Is the app logged in with a user account? */
    val isLoggedIn; get() = accessToken != null && userId != null

    fun logout() {
        setSession()
        if (mainActivity != null) {
            if (!(mainActivity!!.isDestroyed)) {
                mainActivity!!.navigate(MobileNavigationDirections.actionToAccount())
            }
        }
    }

    var useDarkTheme
        get() = preferences.getBoolean(KEY_USE_DARK_THEME, true)
        set(value) = preferences.edit().putBoolean(KEY_USE_DARK_THEME, value).apply()

    /** The Jumble-X-Sale API. */
    val api by lazy { JumbleApi.create(this) }

    /** Repository for user messages received. */
    val userMessages by lazy { UserMessageRepository(this) }

    /** Repository for sale offer messages received. */
    val saleOfferMessages by lazy { SaleOfferMessageRepository(this) }

    /** Repository for completd sale messages received. */
    val completedSaleMessages by lazy { CompletedSaleMessageRepository(this) }

    /** Repository for sale offers from other members. */
    val othersSaleOffers by lazy { OthersSaleOfferRepository(this) }

    /** Repository for completed sales from other members. */
    val othersCompletedSales by lazy { OthersCompletedSaleRepository(this) }

    /** Repository for sale offers from this member. */
    val ownSaleOffers by lazy { OwnSaleOfferRepository(this) }

    /** Repository for sale offers from this member. */
    val ownCompletedSales by lazy { OwnCompletedSaleRepository(this) }

    /** Repository for secret santa offers from other members. */
    val takeableSecretSantaOffers by lazy { TakeableSecretSantaOfferRepository(this) }

    /** Repository for secret santa offers from this member. */
    val givableSecretSantaOffers by lazy { GivableSecretSantaOfferRepository(this) }

    /** Repository for completed secret santas for this member. */
    val givenAndTakenSecretSantas by lazy { GivenAndTakenSecretSantaRepository(this) }

    val moshi: Moshi by lazy {
        Moshi.Builder()
                .add(GenericCollectionAdapterFactory(ArrayList::class.java) { ArrayList() })
                .add(UUIDJsonAdapter())
                .add(InstantJsonAdapter())
                .add(BigDecimalJsonAdapter())
                .add(PolymorphicJsonAdapterFactory.of(Filter::class.java, "operator")
                        .withSubtype(SingleValueFilter::class.java, SingleValueFilter.Operator.EQUAL_TO.code)
                        .withSubtype(SingleValueFilter::class.java, SingleValueFilter.Operator.NOT_EQUAL_TO.code)
                        .withSubtype(SingleValueFilter::class.java, SingleValueFilter.Operator.LESS_THAN_OR_EQUAL_TO.code)
                        .withSubtype(SingleValueFilter::class.java, SingleValueFilter.Operator.GREATER_THAN_OR_EQUAL_TO.code)
                        .withSubtype(SingleValueFilter::class.java, SingleValueFilter.Operator.LESS_THAN.code)
                        .withSubtype(SingleValueFilter::class.java, SingleValueFilter.Operator.GREATER_THAN.code)
                        .withSubtype(DualValueFilter::class.java, DualValueFilter.Operator.BETWEEN.code)
                        .withSubtype(DualValueFilter::class.java, DualValueFilter.Operator.IN_BETWEEN.code)
                        .withSubtype(MultiValueFilter::class.java, MultiValueFilter.Operator.IN.code)
                        .withSubtype(MultiValueFilter::class.java, MultiValueFilter.Operator.NOT_IN.code))
                .build()
    }

    fun hideKeyboardFrom(view: View) {
        val inputMethodManager = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    companion object {
        @JvmStatic private val TAG = JumbleApplication::class.java.simpleName
        private const val KEY_USE_DARK_THEME = "useDarkTheme"
        private const val KEY_USER_ID = "userId"
        private const val KEY_EMAIL = "email"
        private const val KEY_PASSWORD = "password"
        private const val KEY_ACCESS_TOKEN = "accessToken"

        @Suppress("ConstantConditionIf")
        @JvmStatic val isDebugLocal = BuildConfig.BUILD_TYPE == "debugLocal"
    }

}
