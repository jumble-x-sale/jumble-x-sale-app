package com.jumble_x_sale.app.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.Nullable
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import com.jumble_x_sale.app.view.MainActivity
import com.jumble_x_sale.app.MobileNavigationDirections
import com.jumble_x_sale.app.R

abstract class LoggedInFragmentBase : FragmentBase() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (!app.isLoggedIn) {
            navigate(MobileNavigationDirections.actionToAccount())
        }
    }

}
