package com.jumble_x_sale.app.view.sale

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.util.formatGerman
import com.jumble_x_sale.app.view.LoggedInFragmentBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.UUID

class PurchaseItemFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: PurchaseItemViewModel
    private var itemId: Int = 0
    private var item: SaleOfferData? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(PurchaseItemViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_purchase_item, container, false)!!

        itemId = arguments?.getInt("itemId") ?: 0
        item = app.othersSaleOffers.data.value?.get(itemId)

        root.findViewById<TextView>(R.id.category_text).apply { text = item?.category }
        root.findViewById<TextView>(R.id.title_text).apply { text = item?.title }
        root.findViewById<TextView>(R.id.description_text).apply { text = item?.description }
        root.findViewById<TextView>(R.id.price_text).apply { text = item?.price.formatGerman() }

        root.findViewById<Button>(R.id.purchase_btn).setOnClickListener { purchase() }

        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.purchase_item, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.do_purchase_item -> purchase()
            R.id.do_send_message -> navigate(PurchaseItemFragmentDirections.actionSendMessage(itemId))
        }
        return super.onOptionsItemSelected(item)
    }

    private fun purchase() {
        if (item?.id == null) {
            purchaseResult(false)
        } else {
            app.api.saleOfferAccept(item!!.id!!).enqueue(object : Callback<UUID> {
                override fun onResponse(call: Call<UUID>?, response: Response<UUID>?) {
                    if (response != null && response.isSuccessful && response.body() != null) {
                        purchaseResult(true)
                    } else {
                        Log.w(TAG, response?.toString() ?: "")
                        purchaseResult(false)
                    }
                }

                override fun onFailure(call: Call<UUID>, t: Throwable) {
                    Log.w(TAG, t.toString())
                    purchaseResult(false)
                }
            })
        }
    }

    private fun purchaseResult(success: Boolean) {
        if (success) {
            app.othersSaleOffers.data.value?.removeAt(itemId)
            Toast.makeText(app, R.string.purchase_done, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(app, R.string.network_error, Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        @JvmStatic private val TAG = PurchaseItemFragment::class.java.simpleName
    }

}
