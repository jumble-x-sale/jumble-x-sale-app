package com.jumble_x_sale.app.view

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions

abstract class ViewModelBase(application: Application) : AndroidViewModel(application) {

    /** The Jumble-X-Sale app. */
    protected val app; get() = this.getApplication<JumbleApplication>()

}
