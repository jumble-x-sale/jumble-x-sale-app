package com.jumble_x_sale.app.view.secret_santa

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.view.LoggedInFragmentBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GivableItemFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: GivableItemViewModel
    private var itemId: Int = 0
    private var givableItem: SecretSantaOfferData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        itemId = arguments?.getInt("itemId") ?: 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(GivableItemViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_givable_item, container, false)!!

        givableItem = app.givableSecretSantaOffers.data.value?.get(itemId)

        bindEditText(root, R.id.category_text, viewModel.category)
        bindEditText(root, R.id.title_text, viewModel.title)
        bindEditText(root, R.id.description_text, viewModel.description)

        viewModel.category.value = givableItem?.category
        viewModel.title.value = givableItem?.title
        viewModel.description.value = givableItem?.description

        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.givable_item, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.change_item -> change()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun change() {
        if (viewModel.category.value == null) {
            Toast.makeText(app, R.string.category_missing, Toast.LENGTH_LONG).show()
        } else if (viewModel.title.value == null) {
            Toast.makeText(app, R.string.title_missing, Toast.LENGTH_LONG).show()
        } else if (viewModel.description.value == null) {
            Toast.makeText(app, R.string.description_missing, Toast.LENGTH_LONG).show()
        } else {
            val data = SecretSantaOfferData(
                id = givableItem?.id,
                category = viewModel.category.value,
                title = viewModel.title.value,
                description = viewModel.description.value)

            app.api.secretSantaOfferUpdate(data).enqueue(object : Callback<Unit> {
                override fun onResponse(call: Call<Unit>?, response: Response<Unit>?) {
                    if (response != null && response.isSuccessful && response.body() != null) {
                        changeResult(data)
                    } else {
                        Log.w(TAG, response?.toString() ?: "")
                        changeResult()
                    }
                }

                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    Log.w(TAG, t.toString())
                    changeResult()
                }
            })
        }
    }

    private fun changeResult(data: SecretSantaOfferData? = null) {
        if (data != null) {
            givableItem?.category = data.category
            givableItem?.title = data.title
            givableItem?.description = data.description
            Toast.makeText(app, R.string.secret_santa_offer_changed, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(app, R.string.network_error, Toast.LENGTH_LONG).show()
        }
        navigateUp()
    }

    companion object {
        @JvmStatic private val TAG = GivableItemFragment::class.java.simpleName
    }

}
