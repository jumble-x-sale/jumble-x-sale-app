package com.jumble_x_sale.app.view

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.DataPage
import com.jumble_x_sale.app.view.LoggedInFragmentBase

abstract class ListFragmentBase<TData, TPage, TRepo, TViewModel, TViewHolder, TAdapter>() : LoggedInFragmentBase() where
    TPage : DataPage<TData>,
    TRepo : com.jumble_x_sale.app.data.repo.RepositoryBase<TData, TPage>,
    TViewModel : ListViewModelBase<TData, TPage, TRepo>,
    TViewHolder : RecyclerView.ViewHolder,
    TAdapter : ListAdapterBase<TData, TPage, TRepo, TViewModel, TViewHolder>
{

    protected abstract var viewModel: TViewModel
    protected var recyclerView: RecyclerView? = null
    protected var recyclerAdapter: TAdapter? = null
    protected var recyclerLayout: RecyclerView.LayoutManager? = null
    private var scrollListener: EndlessRecyclerViewScrollListener? = null

    override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(itemView, savedInstanceState)

        val recyclerView = getRecycler()
        val recyclerAdapter = getAdapter()
        recyclerView.apply {
            layoutManager = getLayout()
            adapter = recyclerAdapter
        }
        viewModel.data.observe(mainActivity, { value -> recyclerAdapter.data = value })
        viewModel.itemSelectedEvent.observe(this) { itemId ->
            navigate(getActionToItem(itemId))
        }
        scrollListener = object : EndlessRecyclerViewScrollListener(recyclerView.layoutManager!!) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                viewModel.loadMore()
            }
        }
        recyclerView.addOnScrollListener(scrollListener!!)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.refresh -> viewModel.refresh()
        }
        return super.onOptionsItemSelected(item)
    }

    protected abstract fun getRecycler(): RecyclerView

    protected abstract fun getAdapter(): TAdapter

    protected open fun getLayout(): RecyclerView.LayoutManager =
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            GridLayoutManager(activity, 2)
        } else { // portrait/square/undefined
            LinearLayoutManager(activity)
        }

    protected abstract fun getActionToItem(itemId: Int): NavDirections

    companion object {
        @JvmStatic private val TAG = ListFragmentBase::class.java.simpleName
    }

}
