package com.jumble_x_sale.app.view.message

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.view.LoggedInFragmentBase

class UserMessageFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: UserMessageViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(UserMessageViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_user_message, container, false)!!

        val textView: TextView = root.findViewById(R.id.text_user_message)
        viewModel.text.observe(viewLifecycleOwner, Observer { textView.text = it })

        return root
    }

    companion object {
        @JvmStatic private val TAG = UserMessageFragment::class.java.simpleName
    }

}
