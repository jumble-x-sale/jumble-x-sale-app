package com.jumble_x_sale.app.view.message

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.UserMessageData
import com.jumble_x_sale.app.data.model.UserMessageDataPage
import com.jumble_x_sale.app.data.repo.UserMessageRepository
import com.jumble_x_sale.app.view.ListFragmentBase

class UserMessageListFragment : ListFragmentBase<
        UserMessageData,
        UserMessageDataPage,
        UserMessageRepository,
        UserMessageListViewModel,
        UserMessageListAdapter.ViewHolder,
        UserMessageListAdapter
>() {

    override lateinit var viewModel: UserMessageListViewModel
    private lateinit var menu: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.user_message_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.user_message_add -> navigate(UserMessageListFragmentDirections.actionCreate())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(UserMessageListViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_user_message_list, container, false)!!

        val addButton = root.findViewById<FloatingActionButton>(R.id.fab_user_message_create)
        addButton.setOnClickListener { navigate(UserMessageListFragmentDirections.actionCreate()) }

        return root
    }

    override fun getRecycler(): RecyclerView = root.findViewById(R.id.list_view_user_message)

    override fun getAdapter() = UserMessageListAdapter(viewModel)

    override fun getActionToItem(itemId: Int) = UserMessageListFragmentDirections.actionToItem(itemId)

    companion object {
        @JvmStatic private val TAG = UserMessageListFragment::class.java.simpleName
    }

}
