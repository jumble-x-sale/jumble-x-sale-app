package com.jumble_x_sale.app.view.sale

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.view.ViewModelBase
import java.math.BigDecimal
import java.time.Instant
import java.util.UUID

class SoldItemViewModel(application: Application) : ViewModelBase(application) {

    val creatorId = MutableLiveData<UUID>()
    val creationInstant = MutableLiveData<Instant>()
    val category = MutableLiveData<String>()
    val title = MutableLiveData<String>()
    val description = MutableLiveData<String>()
    val mainImageData = MutableLiveData<ByteArray>()
    val price = MutableLiveData<BigDecimal>()

}
