package com.jumble_x_sale.app.view.secret_santa

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.CompletedSecretSantaData
import com.jumble_x_sale.app.data.model.CompletedSecretSantaDataPage
import com.jumble_x_sale.app.data.repo.GivenAndTakenSecretSantaRepository
import com.jumble_x_sale.app.view.ListFragmentBase

class GivenAndTakenListFragment : ListFragmentBase<
        CompletedSecretSantaData,
        CompletedSecretSantaDataPage,
        GivenAndTakenSecretSantaRepository,
        GivenAndTakenListViewModel,
        GivenAndTakenListAdapter.ViewHolder,
        GivenAndTakenListAdapter
>() {

    override lateinit var viewModel: GivenAndTakenListViewModel
    private lateinit var menu: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.given_and_taken_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.to_givable_list -> navigate(GivenAndTakenListFragmentDirections.actionToGivableList())
            R.id.to_takeable_list -> navigate(GivenAndTakenListFragmentDirections.actionToTakeableList())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(GivenAndTakenListViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_given_and_taken_list, container, false)!!

        return root
    }

    override fun getRecycler(): RecyclerView = root.findViewById(R.id.list_view_given_and_taken)

    override fun getAdapter() = GivenAndTakenListAdapter(viewModel)

    override fun getActionToItem(itemId: Int) = GivenAndTakenListFragmentDirections.actionToItem(itemId)

    companion object {
        @JvmStatic private val TAG = GivenAndTakenListFragment::class.java.simpleName
    }

}
