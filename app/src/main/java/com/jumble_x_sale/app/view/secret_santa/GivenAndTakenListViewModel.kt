package com.jumble_x_sale.app.view.secret_santa

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.CompletedSecretSantaData
import com.jumble_x_sale.app.data.model.CompletedSecretSantaDataPage
import com.jumble_x_sale.app.data.repo.GivenAndTakenSecretSantaRepository
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.view.ListViewModelBase

class GivenAndTakenListViewModel(
    application: Application
) : ListViewModelBase<CompletedSecretSantaData, CompletedSecretSantaDataPage, GivenAndTakenSecretSantaRepository>(application) {

    override var repo: GivenAndTakenSecretSantaRepository = app.givenAndTakenSecretSantas

    companion object {
        @JvmStatic private val TAG = GivenAndTakenListViewModel::class.java.simpleName
    }

}
