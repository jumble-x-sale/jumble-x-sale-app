package com.jumble_x_sale.app.view

import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.data.model.DataPage

abstract class ListAdapterBase<TData, TPage, TRepo, TViewModel, TViewHolder>(
    protected val viewModel: TViewModel
) : RecyclerView.Adapter<TViewHolder>() where
    TPage : DataPage<TData>,
    TRepo : com.jumble_x_sale.app.data.repo.RepositoryBase<TData, TPage>,
    TViewModel : ListViewModelBase<TData, TPage, TRepo>,
    TViewHolder : RecyclerView.ViewHolder
{

    var data: MutableList<TData>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return viewModel.data.value?.size ?: 0
    }

}
