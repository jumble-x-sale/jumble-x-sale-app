package com.jumble_x_sale.app.view.sale

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.data.model.SaleOfferDataPage
import com.jumble_x_sale.app.data.repo.OwnSaleOfferRepository
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.view.ListViewModelBase

class SellListViewModel(
    application: Application
) : ListViewModelBase<SaleOfferData, SaleOfferDataPage, OwnSaleOfferRepository>(application) {

    override var repo: OwnSaleOfferRepository = app.ownSaleOffers

    companion object {
        @JvmStatic private val TAG = SellListViewModel::class.java.simpleName
    }

}
