package com.jumble_x_sale.app.view.secret_santa

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.data.model.SecretSantaOfferDataPage
import com.jumble_x_sale.app.data.repo.GivableSecretSantaOfferRepository
import com.jumble_x_sale.app.util.Utils
import com.jumble_x_sale.app.util.formatGerman
import com.jumble_x_sale.app.view.ListAdapterBase
import java.util.Locale

class GivableListAdapter(
    viewModel: GivableListViewModel
) : ListAdapterBase<
        SecretSantaOfferData,
        SecretSantaOfferDataPage,
        GivableSecretSantaOfferRepository,
        GivableListViewModel,
        GivableListAdapter.ViewHolder
>(viewModel) {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val titleView: TextView = itemView.findViewById(R.id.title_text)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.list_item_givable, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        if (data != null) {
            val item = data?.get(i)!!
            viewHolder.titleView.text = item.title

            viewHolder.itemView.setOnClickListener { _ -> viewModel.setItemSelected(i) }
        }
    }

}
