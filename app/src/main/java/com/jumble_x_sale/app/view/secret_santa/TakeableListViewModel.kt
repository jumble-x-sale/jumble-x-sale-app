package com.jumble_x_sale.app.view.secret_santa

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.data.model.SecretSantaOfferDataPage
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.data.repo.TakeableSecretSantaOfferRepository
import com.jumble_x_sale.app.view.ListViewModelBase

class TakeableListViewModel(
    application: Application
) : ListViewModelBase<SecretSantaOfferData, SecretSantaOfferDataPage, TakeableSecretSantaOfferRepository>(application) {

    override var repo: TakeableSecretSantaOfferRepository = app.takeableSecretSantaOffers

    companion object {
        @JvmStatic private val TAG = TakeableListViewModel::class.java.simpleName
    }

}
