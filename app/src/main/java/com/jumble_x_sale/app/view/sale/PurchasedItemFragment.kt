package com.jumble_x_sale.app.view.sale

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.util.formatGerman
import com.jumble_x_sale.app.view.LoggedInFragmentBase

class PurchasedItemFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: PurchasedItemViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(PurchasedItemViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_purchased_item, container, false)!!

        val item = app.othersCompletedSales.data.value?.get(arguments?.getInt("itemId") ?: 0)

        root.findViewById<TextView>(R.id.category_text).apply { text = item?.category }
        root.findViewById<TextView>(R.id.title_text).apply { text = item?.title }
        root.findViewById<TextView>(R.id.description_text).apply { text = item?.description }
        root.findViewById<TextView>(R.id.price_text).apply { text = item?.price.formatGerman() }

        return root
    }

    override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(itemView, savedInstanceState)
    }

    companion object {
        @JvmStatic private val TAG = PurchasedItemFragment::class.java.simpleName
    }

}
