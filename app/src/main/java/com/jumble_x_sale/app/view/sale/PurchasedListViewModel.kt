package com.jumble_x_sale.app.view.sale

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.CompletedSaleDataPage
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import com.jumble_x_sale.app.data.repo.OthersCompletedSaleRepository
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.util.SingleLiveEvent
import com.jumble_x_sale.app.view.ListViewModelBase
import java.math.BigDecimal
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PurchasedListViewModel(
    application: Application
) : ListViewModelBase<CompletedSaleData, CompletedSaleDataPage, OthersCompletedSaleRepository>(application) {

    override var repo: OthersCompletedSaleRepository = app.othersCompletedSales

    companion object {
        @JvmStatic private val TAG = PurchasedListViewModel::class.java.simpleName
    }

}
