package com.jumble_x_sale.app.view.secret_santa

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.view.LoggedInFragmentBase

class TakeableItemFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: TakeableItemViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(TakeableItemViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_takeable_item, container, false)!!

        val item = app.takeableSecretSantaOffers.data.value?.get(arguments?.getInt("itemId") ?: 0)

        root.findViewById<TextView>(R.id.category_text).apply { text = item?.category }
        root.findViewById<TextView>(R.id.title_text).apply { text = item?.title }
        root.findViewById<TextView>(R.id.description_text).apply { text = item?.description }

        return root
    }

    companion object {
        @JvmStatic private val TAG = TakeableItemFragment::class.java.simpleName
    }

}
