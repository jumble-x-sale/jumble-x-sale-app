package com.jumble_x_sale.app.view.message

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.UserMessageData
import com.jumble_x_sale.app.data.model.UserMessageDataPage
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.data.repo.UserMessageRepository
import com.jumble_x_sale.app.view.ListViewModelBase

class UserMessageListViewModel(
    application: Application
) : ListViewModelBase<UserMessageData, UserMessageDataPage, UserMessageRepository>(application) {

    override var repo: UserMessageRepository = app.userMessages

    companion object {
        @JvmStatic private val TAG = UserMessageListViewModel::class.java.simpleName
    }

}
