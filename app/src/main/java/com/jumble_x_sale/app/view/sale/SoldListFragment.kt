package com.jumble_x_sale.app.view.sale

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.CompletedSaleDataPage
import com.jumble_x_sale.app.data.repo.OwnCompletedSaleRepository
import com.jumble_x_sale.app.view.EndlessRecyclerViewScrollListener
import com.jumble_x_sale.app.view.ListFragmentBase

class SoldListFragment : ListFragmentBase<
        CompletedSaleData,
        CompletedSaleDataPage,
        OwnCompletedSaleRepository,
        SoldListViewModel,
        SoldListAdapter.ViewHolder,
        SoldListAdapter
>() {

    override lateinit var viewModel: SoldListViewModel
    private lateinit var menu: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.sold_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.to_sell_list -> navigate(SoldListFragmentDirections.actionToSellList())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(SoldListViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_sold_list, container, false)!!

        return root
    }

    override fun getRecycler(): RecyclerView = root.findViewById(R.id.list_view_sold)

    override fun getAdapter() = SoldListAdapter(viewModel)

    override fun getActionToItem(itemId: Int) = SoldListFragmentDirections.actionToItem(itemId)

    companion object {
        @JvmStatic private val TAG = SoldListFragment::class.java.simpleName
    }

}
