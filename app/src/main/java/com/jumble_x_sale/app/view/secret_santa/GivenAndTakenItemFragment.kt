package com.jumble_x_sale.app.view.secret_santa

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.view.LoggedInFragmentBase

class GivenAndTakenItemFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: GivenAndTakenItemViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(GivenAndTakenItemViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_given_and_taken_item, container, false)!!

        val item = app.givenAndTakenSecretSantas.data.value?.get(arguments?.getInt("itemId") ?: 0)

        root.findViewById<TextView>(R.id.category1_text).apply { text = item?.category1 }
        root.findViewById<TextView>(R.id.title1_text).apply { text = item?.title1 }
        root.findViewById<TextView>(R.id.description1_text).apply { text = item?.description1 }

        root.findViewById<TextView>(R.id.category2_text).apply { text = item?.category2 }
        root.findViewById<TextView>(R.id.title2_text).apply { text = item?.title2 }
        root.findViewById<TextView>(R.id.description2_text).apply { text = item?.description2 }

        return root
    }

    companion object {
        @JvmStatic private val TAG = GivenAndTakenItemFragment::class.java.simpleName
    }

}
