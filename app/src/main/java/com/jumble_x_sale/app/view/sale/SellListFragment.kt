package com.jumble_x_sale.app.view.sale

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.data.model.SaleOfferDataPage
import com.jumble_x_sale.app.data.repo.OwnSaleOfferRepository
import com.jumble_x_sale.app.view.EndlessRecyclerViewScrollListener
import com.jumble_x_sale.app.view.ListFragmentBase

class SellListFragment : ListFragmentBase<
        SaleOfferData,
        SaleOfferDataPage,
        OwnSaleOfferRepository,
        SellListViewModel,
        SellListAdapter.ViewHolder,
        SellListAdapter
>() {

    override lateinit var viewModel: SellListViewModel
    private lateinit var menu: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.sell_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.to_sold_list -> navigate(SellListFragmentDirections.actionToSoldList())
            R.id.add_item -> navigate(SellListFragmentDirections.actionCreate())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(SellListViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_sell_list, container, false)!!

        val addButton = root.findViewById<FloatingActionButton>(R.id.fab_sale_offer_create)
        addButton.setOnClickListener { navigate(SellListFragmentDirections.actionCreate()) }

        return root
    }

    override fun getRecycler(): RecyclerView = root.findViewById(R.id.list_view_sell)

    override fun getAdapter() = SellListAdapter(viewModel)

    override fun getActionToItem(itemId: Int) = SellListFragmentDirections.actionToItem(itemId)

    companion object {
        @JvmStatic private val TAG = SellListFragment::class.java.simpleName
    }

}
