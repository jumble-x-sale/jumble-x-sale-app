package com.jumble_x_sale.app.view.secret_santa

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class TakeableItemViewModel(application: Application) : AndroidViewModel(application) {

    private val _text = MutableLiveData<String>().apply {
        value = "This is the Secret Santa Offer Fragment"
    }
    val text: LiveData<String> = _text

}
