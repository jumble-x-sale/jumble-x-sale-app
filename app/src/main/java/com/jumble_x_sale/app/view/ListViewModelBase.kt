package com.jumble_x_sale.app.view

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.DataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.util.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class ListViewModelBase<TData, TPage, TRepo>(
    application: Application
) : ViewModelBase(application) where
    TPage : DataPage<TData>,
    TRepo : RepositoryBase<TData, TPage>
{

    abstract var repo: TRepo
        protected set

    val filterPagingOptions = MutableLiveData(mutableListOf<Filter>()).apply { FilterPagingOptions() }
    val data by lazy { repo.data }

    val itemSelectedEvent = SingleLiveEvent<Int>()
    fun setItemSelected(itemId: Int) { itemSelectedEvent.value = itemId }

    /** Refresh data. */
    fun refresh() { repo.refresh() }

    /** Load more data. */
    fun loadMore() { repo.loadMore() }

    companion object {
        @JvmStatic private val TAG = ListViewModelBase::class.java.simpleName
    }

}
