package com.jumble_x_sale.app.view.message

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class UserMessageItemViewModel(application: Application) : AndroidViewModel(application) {

    val message = MutableLiveData<String>()

}
