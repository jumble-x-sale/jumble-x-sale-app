package com.jumble_x_sale.app.view.sale

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.data.model.SaleOfferDataPage
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import com.jumble_x_sale.app.data.repo.OthersSaleOfferRepository
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.util.SingleLiveEvent
import com.jumble_x_sale.app.view.ListViewModelBase
import java.math.BigDecimal
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PurchaseListViewModel(
    application: Application
) : ListViewModelBase<SaleOfferData, SaleOfferDataPage, OthersSaleOfferRepository>(application) {

    override var repo: OthersSaleOfferRepository = app.othersSaleOffers

    companion object {
        @JvmStatic private val TAG = PurchaseListViewModel::class.java.simpleName
    }

}
