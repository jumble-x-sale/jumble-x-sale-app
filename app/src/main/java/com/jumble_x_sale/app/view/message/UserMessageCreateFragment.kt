package com.jumble_x_sale.app.view.message

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.UserMessageData
import com.jumble_x_sale.app.view.LoggedInFragmentBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.Instant
import java.util.UUID

class UserMessageCreateFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: UserMessageCreateViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(UserMessageCreateViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_user_message_create, container, false)!!

        bindEditText(root, R.id.message_text, viewModel.message)

        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.user_message_create, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.user_message_create_done -> complete()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun complete() {
        if (viewModel.message.value == null) {
            Toast.makeText(app, R.string.message_missing, Toast.LENGTH_LONG).show()
        } else {
            // Giving the option to choose a proper recipient is work that remains to be done.
            // So all we do for now is to create a message for oneself.
            val data = UserMessageData(
                senderId = app.userId,
                creationInstant = Instant.now(),
                message = viewModel.message.value,
                receiverId = app.userId)

            app.api.userMessageCreate(data).enqueue(object : Callback<UUID> {
                override fun onResponse(call: Call<UUID>?, response: Response<UUID>?) {
                    if (response != null && response.isSuccessful && response.body() != null) {
                        completeResult(data)
                    } else {
                        Log.w(TAG, response?.toString() ?: "")
                        completeResult()
                    }
                }

                override fun onFailure(call: Call<UUID>, t: Throwable) {
                    Log.w(TAG, t.toString())
                    completeResult()
                }
            })
        /*
        } else {
            Toast.makeText(app, R.string.internal_error, Toast.LENGTH_LONG).show()
        */
        }
    }

    private fun completeResult(data: UserMessageData? = null) {
        if (data != null) {
            app.userMessages.data.value?.add(data)
            Toast.makeText(app, R.string.message_sent, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(app, R.string.network_error, Toast.LENGTH_LONG).show()
        }
        navigateUp()
    }

    companion object {
        @JvmStatic private val TAG = UserMessageCreateFragment::class.java.simpleName
    }

}
