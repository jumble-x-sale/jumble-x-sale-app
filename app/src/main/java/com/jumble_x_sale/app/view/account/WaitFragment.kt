package com.jumble_x_sale.app.view.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.view.LoggedInFragmentBase

class WaitFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: WaitViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(WaitViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_wait_for_login, container, false)!!

        val textView: TextView = root.findViewById(R.id.text_wait)
        viewModel.text.observe(viewLifecycleOwner, Observer { textView.text = it })

        root.isEnabled = false

        return root
    }

    companion object {
        @JvmStatic private val TAG = WaitFragment::class.java.simpleName
    }

}
