package com.jumble_x_sale.app.view.account

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class RegisterViewModel(application: Application) : AndroidViewModel(application) {

    val firstName = MutableLiveData<String>() // Max
    val lastName = MutableLiveData<String>() // Mustermann
    val email = MutableLiveData<String>() // max@mustermann.de
    val password = MutableLiveData<String>() // 1234
    val idCard = MutableLiveData<String>() // for example: "lhm310k1g398092282302096"

}
