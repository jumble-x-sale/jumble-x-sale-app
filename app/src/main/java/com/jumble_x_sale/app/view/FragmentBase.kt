package com.jumble_x_sale.app.view

import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.MenuItem
import android.widget.EditText
import android.widget.ImageView
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import com.jumble_x_sale.app.view.MainActivity
import com.jumble_x_sale.app.util.formatGerman
import com.jumble_x_sale.app.util.parseBigDecimalGerman
import java.math.BigDecimal

abstract class FragmentBase : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (isKeyboardOpen()) {
            hideKeyboard()
            return true
        }
        return false
    }

    protected lateinit var root: View
    protected val mainActivity; get() = activity as MainActivity

    /** The Jumble-X-Sale app. */
    protected val app; get() = mainActivity.app

    /** Is the keyboard currently open? */
    protected fun isKeyboardOpen() = mainActivity.isKeyboardOpen()

    /** Hide the keyboard. */
    protected fun hideKeyboard() = mainActivity.hideKeyboard()

    protected fun setTitle(titleId: Int) {
        mainActivity.toolbar.title = app.resources.getString(titleId)
    }

    fun navigate(navDirections: NavDirections, ignoreNavStack: Boolean = true) {
        mainActivity.navigate(navDirections, ignoreNavStack)
    }

    fun navigateUp() = mainActivity.navigateUp()

    protected fun bindEditText(root: View, @IdRes id: Int, liveData: MutableLiveData<String>): EditText {
        val editText: EditText = root.findViewById(id)
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                liveData.value = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        liveData.observe(viewLifecycleOwner, Observer {
            if (editText.isAttachedToWindow && editText.text.toString() != it) {
                editText.setText(it)
            }
        })
        return editText
    }

    protected fun bindEditNumber(root: View, @IdRes id: Int, liveData: MutableLiveData<BigDecimal>): EditText {
        val editText: EditText = root.findViewById(id)
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                liveData.value = s.toString().parseBigDecimalGerman()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        liveData.observe(viewLifecycleOwner, Observer {
            val formatGerman = it.formatGerman()
            if (editText.isAttachedToWindow && editText.text.toString().parseBigDecimalGerman() != it) {
                editText.setText(it.formatGerman())
            }
        })
        return editText
    }

    protected fun bindEditImage(
        root: View,
        @IdRes id: Int,
        liveData: MutableLiveData<Bitmap>,
        prepareBitmap: BitmapTransformer?
    ): ImageView {
        val imageView: ImageView = root.findViewById(id)
        imageView.setOnClickListener {
            mainActivity.dispatchTakePictureIntent { bitmap ->
                liveData.value = bitmap
                imageView.setImageBitmap(prepareBitmap?.invoke(bitmap) ?: bitmap)
            }
        }
        liveData.observe(viewLifecycleOwner, Observer {
            if (imageView.isAttachedToWindow) {
                imageView.setImageBitmap(it)
            }
        })
        return imageView
    }

}

typealias BitmapTransformer = (Bitmap?) -> Bitmap?
