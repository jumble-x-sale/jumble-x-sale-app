package com.jumble_x_sale.app.view.sale

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.data.model.SaleOfferDataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.repo.OthersSaleOfferRepository
import com.jumble_x_sale.app.view.ListFragmentBase
import com.jumble_x_sale.app.view.sale.PurchaseListFragmentDirections

class PurchaseListFragment : ListFragmentBase<
        SaleOfferData,
        SaleOfferDataPage,
        OthersSaleOfferRepository,
        PurchaseListViewModel,
        PurchaseListAdapter.ViewHolder,
        PurchaseListAdapter
>() {

    override lateinit var viewModel: PurchaseListViewModel
    private lateinit var menu: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.purchase_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.to_purchased_list -> navigate(PurchaseListFragmentDirections.actionToPurchasedList())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(PurchaseListViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_purchase_list, container, false)!!

        if (viewModel.filterPagingOptions.value == null) {
            viewModel.filterPagingOptions.value = mutableListOf<Filter>()
        }

        // app.api.userGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<UserData> {
        //     override fun onResponse(call: Call<UserData>?, response: Response<UserData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.userGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<UserData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.userMessageGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<UserMessageData> {
        //     override fun onResponse(call: Call<UserMessageData>?, response: Response<UserMessageData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.userMessageGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<UserMessageData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.saleOfferGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<SaleOfferDataPage> {
        //     override fun onResponse(call: Call<SaleOfferDataPage>?, response: Response<SaleOfferDataPage>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.saleOfferGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<SaleOfferDataPage?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.saleOfferMessageGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<SaleOfferMessageData> {
        //     override fun onResponse(call: Call<SaleOfferMessageData>?, response: Response<SaleOfferMessageData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.saleOfferMessageGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<SaleOfferMessageData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.saleCompletedGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<CompletedSaleData> {
        //     override fun onResponse(call: Call<CompletedSaleData>?, response: Response<CompletedSaleData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.saleCompletedGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<CompletedSaleData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.saleCompletedMessageGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<CompletedSaleMessageData> {
        //     override fun onResponse(call: Call<CompletedSaleMessageData>?, response: Response<CompletedSaleMessageData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.saleCompletedMessageGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<CompletedSaleMessageData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.secretSantaOfferGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<PurchaseOfferData> {
        //     override fun onResponse(call: Call<PurchaseOfferData>?, response: Response<PurchaseOfferData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.secretSantaOfferGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<PurchaseOfferData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.secretSantaCompletedGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<CompletedPurchaseData> {
        //         override fun onResponse(call: Call<CompletedPurchaseData>?, response: Response<CompletedPurchaseData>?) {
        //             if (response != null && response.isSuccessful && response.body() != null) {
        //                 Log.i(TAG, "app.api.secretSantaCompletedGet: OK")
        //             } else {
        //                 Log.w(TAG, response?.toString() ?: "")
        //             }
        //         }

        //         override fun onFailure(call: Call<CompletedPurchaseData?>?, t: Throwable?) {
        //             Log.w(TAG, t.toString())
        //         }
        //     })

        return root
    }

    // override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {
    //     super.onViewCreated(itemView, savedInstanceState)
    // }

    override fun getRecycler(): RecyclerView = root.findViewById(R.id.list_view_purchase)

    override fun getAdapter() = PurchaseListAdapter(viewModel)

    override fun getActionToItem(itemId: Int) = PurchaseListFragmentDirections.actionToItem(itemId)

    companion object {
        @JvmStatic private val TAG = PurchaseListFragment::class.java.simpleName
    }

}
