package com.jumble_x_sale.app.view.account

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.R

class WaitViewModel(application: Application) : AndroidViewModel(application) {

    private val _text = MutableLiveData<String>(application.resources.getString(R.string.wait_for_login))
    val text: LiveData<String> = _text

}
