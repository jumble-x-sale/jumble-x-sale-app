package com.jumble_x_sale.app.view.message

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.CompletedSaleMessageData
import com.jumble_x_sale.app.data.model.CompletedSaleMessageDataPage
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.data.repo.CompletedSaleMessageRepository
import com.jumble_x_sale.app.view.ListViewModelBase

class CompletedSaleMessageListViewModel(
    application: Application
) : ListViewModelBase<CompletedSaleMessageData, CompletedSaleMessageDataPage, CompletedSaleMessageRepository>(application) {

    override var repo: CompletedSaleMessageRepository = app.completedSaleMessages

    companion object {
        @JvmStatic private val TAG = CompletedSaleMessageListViewModel::class.java.simpleName
    }

}
