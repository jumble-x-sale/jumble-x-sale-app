package com.jumble_x_sale.app.view.sale

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import java.math.BigDecimal

class SaleOfferCreateViewModel(application: Application) : AndroidViewModel(application) {

    val category = MutableLiveData<String>()
    val title = MutableLiveData<String>()
    val description = MutableLiveData<String>()
    val price = MutableLiveData<BigDecimal>()
    val photo = MutableLiveData<Bitmap>()

}
