package com.jumble_x_sale.app.view.message

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.data.model.SaleOfferMessageData
import com.jumble_x_sale.app.data.model.SaleOfferMessageDataPage
import com.jumble_x_sale.app.data.repo.SaleOfferMessageRepository
import com.jumble_x_sale.app.util.Utils
import com.jumble_x_sale.app.util.formatGerman
import com.jumble_x_sale.app.view.ListAdapterBase
import java.util.Locale

class SaleMessageListAdapter(
    viewModel: SaleMessageListViewModel
) : ListAdapterBase<
        SaleOfferMessageData,
        SaleOfferMessageDataPage,
        SaleOfferMessageRepository,
        SaleMessageListViewModel,
        SaleMessageListAdapter.ViewHolder
>(viewModel) {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val messageView: TextView = itemView.findViewById(R.id.message_text)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.list_item_user_message, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        if (data != null) {
            val item = data?.get(i)!!
            viewHolder.messageView.text = item.message

            viewHolder.itemView.setOnClickListener { _ -> viewModel.setItemSelected(i) }
        }
    }

}
