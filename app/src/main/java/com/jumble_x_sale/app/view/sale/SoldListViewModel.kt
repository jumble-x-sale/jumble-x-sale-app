package com.jumble_x_sale.app.view.sale

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.CompletedSaleDataPage
import com.jumble_x_sale.app.data.repo.OwnCompletedSaleRepository
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.view.ListViewModelBase

class SoldListViewModel(
    application: Application
) : ListViewModelBase<CompletedSaleData, CompletedSaleDataPage, OwnCompletedSaleRepository>(application) {

    override var repo: OwnCompletedSaleRepository = app.ownCompletedSales

    companion object {
        @JvmStatic private val TAG = SoldListViewModel::class.java.simpleName
    }

}
