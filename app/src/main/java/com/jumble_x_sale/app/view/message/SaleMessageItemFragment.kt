package com.jumble_x_sale.app.view.message

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.view.LoggedInFragmentBase

class SaleMessageItemFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: SaleMessageItemViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(SaleMessageItemViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_user_message_item, container, false)!!

        val item = app.saleOfferMessages.data.value?.get(arguments?.getInt("itemId") ?: 0)

        root.findViewById<TextView>(R.id.text_view_message).apply { text = item?.message }

        return root
    }

    companion object {
        @JvmStatic private val TAG = SaleMessageItemFragment::class.java.simpleName
    }

}
