package com.jumble_x_sale.app.view.account

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.view.FragmentBase

class AccountFragment : FragmentBase() {

    private lateinit var viewModel: AccountViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!app.isLoggedIn) {
            navigate(AccountFragmentDirections.actionToLogin())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(AccountViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_account, container, false)!!

        val logoutButton: Button = root.findViewById(R.id.logout_btn)
        logoutButton.setOnClickListener { logout() }

        val loggedInAsTextView: TextView = root.findViewById(R.id.text_loggedin_as)
        if (app.isLoggedIn) {
            loggedInAsTextView.text = String.format(getString(R.string.loggedin_as), app.email)
            loggedInAsTextView.visibility = View.VISIBLE
        } else {
            loggedInAsTextView.visibility = View.INVISIBLE
        }

        return root
    }

    private fun logout() {
        app.setSession()
        navigate(AccountFragmentDirections.actionToLogin())
    }

    companion object {
        @JvmStatic private val TAG = AccountFragment::class.java.simpleName
    }

}
