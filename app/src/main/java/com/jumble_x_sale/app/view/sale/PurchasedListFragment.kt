package com.jumble_x_sale.app.view.sale

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.CompletedSaleDataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.repo.OthersCompletedSaleRepository
import com.jumble_x_sale.app.view.ListFragmentBase
import com.jumble_x_sale.app.view.sale.PurchasedListFragmentDirections

class PurchasedListFragment : ListFragmentBase<
        CompletedSaleData,
        CompletedSaleDataPage,
        OthersCompletedSaleRepository,
        PurchasedListViewModel,
        PurchasedListAdapter.ViewHolder,
        PurchasedListAdapter
>() {

    override lateinit var viewModel: PurchasedListViewModel
    private lateinit var menu: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.purchased_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.to_purchase_list -> navigate(PurchasedListFragmentDirections.actionToPurchaseList())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(PurchasedListViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_purchased_list, container, false)!!

        if (viewModel.filterPagingOptions.value == null) {
            viewModel.filterPagingOptions.value = mutableListOf<Filter>()
        }

        // app.api.userGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<UserData> {
        //     override fun onResponse(call: Call<UserData>?, response: Response<UserData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.userGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<UserData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.userMessageGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<UserMessageData> {
        //     override fun onResponse(call: Call<UserMessageData>?, response: Response<UserMessageData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.userMessageGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<UserMessageData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.saleOfferGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<CompletedSaleDataPage> {
        //     override fun onResponse(call: Call<CompletedSaleDataPage>?, response: Response<CompletedSaleDataPage>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.saleOfferGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<CompletedSaleDataPage?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.saleOfferMessageGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<SaleOfferMessageData> {
        //     override fun onResponse(call: Call<SaleOfferMessageData>?, response: Response<SaleOfferMessageData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.saleOfferMessageGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<SaleOfferMessageData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.saleCompletedGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<CompletedSaleData> {
        //     override fun onResponse(call: Call<CompletedSaleData>?, response: Response<CompletedSaleData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.saleCompletedGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<CompletedSaleData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.saleCompletedMessageGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<CompletedSaleMessageData> {
        //     override fun onResponse(call: Call<CompletedSaleMessageData>?, response: Response<CompletedSaleMessageData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.saleCompletedMessageGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<CompletedSaleMessageData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.secretSantaOfferGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<PurchasedOfferData> {
        //     override fun onResponse(call: Call<PurchasedOfferData>?, response: Response<PurchasedOfferData>?) {
        //         if (response != null && response.isSuccessful && response.body() != null) {
        //             Log.i(TAG, "app.api.secretSantaOfferGet: OK")
        //         } else {
        //             Log.w(TAG, response?.toString() ?: "")
        //         }
        //     }

        //     override fun onFailure(call: Call<PurchasedOfferData?>?, t: Throwable?) {
        //         Log.w(TAG, t.toString())
        //     }
        // })

        // app.api.secretSantaCompletedGet(viewModel.filterPagingOptions.value!!)
        //     .enqueue(object : Callback<CompletedPurchasedData> {
        //         override fun onResponse(call: Call<CompletedPurchasedData>?, response: Response<CompletedPurchasedData>?) {
        //             if (response != null && response.isSuccessful && response.body() != null) {
        //                 Log.i(TAG, "app.api.secretSantaCompletedGet: OK")
        //             } else {
        //                 Log.w(TAG, response?.toString() ?: "")
        //             }
        //         }

        //         override fun onFailure(call: Call<CompletedPurchasedData?>?, t: Throwable?) {
        //             Log.w(TAG, t.toString())
        //         }
        //     })

        return root
    }

    // override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {
    //     super.onViewCreated(itemView, savedInstanceState)
    // }

    override fun getRecycler(): RecyclerView = root.findViewById(R.id.list_view_purchased)

    override fun getAdapter() = PurchasedListAdapter(viewModel)

    override fun getActionToItem(itemId: Int) = PurchasedListFragmentDirections.actionToItem(itemId)

    companion object {
        @JvmStatic private val TAG = PurchasedListFragment::class.java.simpleName
    }

}
