package com.jumble_x_sale.app.view.secret_santa

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.data.model.SecretSantaOfferDataPage
import com.jumble_x_sale.app.data.repo.GivableSecretSantaOfferRepository
import com.jumble_x_sale.app.view.ListFragmentBase

class GivableListFragment : ListFragmentBase<
        SecretSantaOfferData,
        SecretSantaOfferDataPage,
        GivableSecretSantaOfferRepository,
        GivableListViewModel,
        GivableListAdapter.ViewHolder,
        GivableListAdapter
>() {

    override lateinit var viewModel: GivableListViewModel
    private lateinit var menu: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.givable_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.to_takeable_list -> navigate(GivableListFragmentDirections.actionToTakeableList())
            R.id.to_given_and_taken_list -> navigate(GivableListFragmentDirections.actionToGivenAndTakenList())
            R.id.add_item -> navigate(GivableListFragmentDirections.actionCreate())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(GivableListViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_givable_list, container, false)!!

        val addButton = root.findViewById<FloatingActionButton>(R.id.fab_secret_santa_offer_create)
        addButton.setOnClickListener { navigate(GivableListFragmentDirections.actionCreate()) }

        return root
    }

    override fun getRecycler(): RecyclerView = root.findViewById(R.id.list_view_givable)

    override fun getAdapter() = GivableListAdapter(viewModel)

    override fun getActionToItem(itemId: Int) = GivableListFragmentDirections.actionToItem(itemId)

    companion object {
        @JvmStatic private val TAG = GivableListFragment::class.java.simpleName
    }

}
