package com.jumble_x_sale.app.view.secret_santa

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.CompletedSecretSantaData
import com.jumble_x_sale.app.data.model.CompletedSecretSantaDataPage
import com.jumble_x_sale.app.data.repo.GivenAndTakenSecretSantaRepository
import com.jumble_x_sale.app.util.Utils
import com.jumble_x_sale.app.util.formatGerman
import com.jumble_x_sale.app.view.ListAdapterBase
import java.util.Locale

class GivenAndTakenListAdapter(
    viewModel: GivenAndTakenListViewModel
) : ListAdapterBase<
        CompletedSecretSantaData,
        CompletedSecretSantaDataPage,
        GivenAndTakenSecretSantaRepository,
        GivenAndTakenListViewModel,
        GivenAndTakenListAdapter.ViewHolder
>(viewModel) {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val title1View: TextView = itemView.findViewById(R.id.title1_text)
        val title2View: TextView = itemView.findViewById(R.id.title2_text)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.list_item_given_and_taken, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        if (data != null) {
            val item = data?.get(i)!!
            viewHolder.title1View.text = item.title1
            viewHolder.title2View.text = item.title2

            viewHolder.itemView.setOnClickListener { _ -> viewModel.setItemSelected(i) }
        }
    }

}
