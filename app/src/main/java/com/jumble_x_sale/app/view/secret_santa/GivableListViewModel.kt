package com.jumble_x_sale.app.view.secret_santa

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.data.model.SecretSantaOfferDataPage
import com.jumble_x_sale.app.data.repo.GivableSecretSantaOfferRepository
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.view.ListViewModelBase

class GivableListViewModel(
    application: Application
) : ListViewModelBase<SecretSantaOfferData, SecretSantaOfferDataPage, GivableSecretSantaOfferRepository>(application) {

    override var repo: GivableSecretSantaOfferRepository = app.givableSecretSantaOffers

    companion object {
        @JvmStatic private val TAG = GivableListViewModel::class.java.simpleName
    }

}
