@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.view.sale.SaleOfferCreateFragment

class MainActivity : AppCompatActivity() {

    lateinit var drawerLayout: DrawerLayout
    lateinit var navController: NavController
    lateinit var navView: NavigationView
    lateinit var navHeaderEmailText: TextView
    lateinit var toolbar: Toolbar
    private lateinit var appBarConfiguration: AppBarConfiguration
    private var lastTakenPhoto: Bitmap? = null

    private var onPhotoTaken: BitmapHandler? = null

    val app; get() = (application!!) as JumbleApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        app.mainActivity = this // This will be the only activity.

        /* TODO:    The nav drawer won't simply show the right coloring in dark mode.
         *          And the app bar instead won't show the right coloring in light mode
         *          (styles with names "Overlay" help to bridge this gap).
         *          >:( Postponed!
         */
        // setTheme(
        //     if (app.useDarkTheme) {
        //         R.style.DarkTheme
        //     } else {
        //         R.style.LightTheme
        //     })

        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                // We don't want the nav stack for entry fragments.
                R.id.nav_home,
                R.id.nav_purchase_list, R.id.nav_purchased_list, R.id.nav_sell_list, R.id.nav_sold_list,
                R.id.nav_takeable_list, R.id.nav_givable_list, R.id.nav_given_and_taken_list,
                R.id.nav_user_message_list, //R.id.nav_sale_message_list,
                R.id.nav_account, R.id.nav_login, R.id.nav_register
            ), drawerLayout
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        navHeaderEmailText = navView.getHeaderView(0).findViewById(R.id.navHeaderEmail)
        updateEmail()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun isKeyboardOpen(): Boolean {
        val rect = Rect()
        drawerLayout.getWindowVisibleDisplayFrame(rect)

        val screenHeight = drawerLayout.rootView.height
        val keypadHeight = screenHeight - rect.bottom

        return keypadHeight > (screenHeight * 0.15)
    }

    fun hideKeyboard() = app.hideKeyboardFrom(drawerLayout)

    @SuppressLint("RtlHardcoded")
    override fun onBackPressed() {
        when {
            drawerLayout.isDrawerOpen(Gravity.LEFT) -> { drawerLayout.closeDrawer(Gravity.LEFT) }
            isKeyboardOpen() -> { hideKeyboard() }
            app.isLoggedIn -> { super.onBackPressed() }
        }
    }

    fun navigate(navDirections: NavDirections, ignoreNavStack: Boolean = true) {
        navController.navigate(navDirections,
            if (ignoreNavStack) {
                ignoreNavStackNavOptions
            } else {
                null
            })
    }

    fun navigateUp() = navController.navigateUp()

    // Update the email shown in the nav header.
    fun updateEmail() {
        navHeaderEmailText.text = app.email ?: getString(R.string.not_logged_in)
    }

    private fun isIntentAvailable(intent: Intent) = intent.resolveActivity(packageManager) != null

    fun dispatchTakePictureIntent(onPhotoTaken: BitmapHandler): Boolean {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (isIntentAvailable(takePictureIntent)) {
            this.onPhotoTaken = onPhotoTaken
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            return true
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            // When no photo was taken for any reason, the bitmap will be null!
                if (resultCode == RESULT_OK) {
                    lastTakenPhoto = intent?.extras?.get("data") as Bitmap?
                    Log.w(TAG, if (lastTakenPhoto != null) { "OK" } else { "EMPTY" })
                } else {
                    lastTakenPhoto = null
                    Log.w(TAG, "ERROR")
                }
            if (lastTakenPhoto != null) {
                onPhotoTaken?.let { it(lastTakenPhoto!!) }
            }
        }
    }

    companion object {
        @JvmStatic private val TAG = MainActivity::class.java.simpleName
        const val REQUEST_IMAGE_CAPTURE = 1
        @JvmStatic private val ignoreNavStackNavOptions = NavOptions.Builder().setLaunchSingleTop(true).build()
    }

}

typealias BitmapHandler = (Bitmap?) -> Unit
