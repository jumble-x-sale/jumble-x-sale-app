package com.jumble_x_sale.app.view

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

/**
 * A [RecyclerView.OnScrollListener] for endless scrolling.
 * [https://gist.github.com/nesquena/d09dc68ff07e845cc622]
 * [https://github.com/codepath/android_guides/wiki/Endless-Scrolling-with-AdapterViews-and-RecyclerView]
 */
abstract class EndlessRecyclerViewScrollListener(
    layoutManager: RecyclerView.LayoutManager,
    /**
     * The minimum amount of items to have below your current scroll position before loading more.
     */
    private var visibleThreshold: Int = 5
) : RecyclerView.OnScrollListener() {

    @Suppress("MemberVisibilityCanBePrivate")
    var layoutManager: RecyclerView.LayoutManager = layoutManager
        set(value) {
            field = value
            if (value is GridLayoutManager) {
                visibleThreshold *= value.spanCount
            } else if (value is StaggeredGridLayoutManager) {
                visibleThreshold *= value.spanCount
            }
        }

    // The current offset index of data you have loaded
    private var currentPage: Int = 0
    // The total number of items in the data set after the last load
    private var previousTotalItemCount: Int = 0
    // True if we are still waiting for the last set of data to load.
    private var loading: Boolean = true
    // Sets the starting page index
    private var startingPageIndex: Int = 0

    private fun getLastVisibleItem(lastVisibleItemPositions: IntArray): Int {
        var maxSize = 0
        for (i in lastVisibleItemPositions.indices) {
            if (i == 0 || lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i]
            }
        }
        return maxSize
    }

    // This happens many times a second during a scroll, so be wary of the code you place here.
    // We are givable a few useful parameters to help us work out if we need to load some more data,
    // but first we check if we are waiting for the previous load to finish.
    override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
        val totalItemCount: Int = layoutManager.itemCount

        val lastVisibleItemPosition: Int = when (layoutManager) {
            is StaggeredGridLayoutManager -> getLastVisibleItem((layoutManager as StaggeredGridLayoutManager).findLastVisibleItemPositions(null))
            else -> (layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
        }

        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex
            this.previousTotalItemCount = totalItemCount
            if (totalItemCount == 0) {
                this.loading = true
            }
        }
        // If it’s still loading, we check to see if the data set count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false
            previousTotalItemCount = totalItemCount
        }

        // If it isn’t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        // threshold should reflect how many total columns there are too
        if (!loading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
            currentPage++
            onLoadMore(currentPage, totalItemCount, view)
            loading = true
        }
    }
    
    // Call this method whenever performing new searches
    fun resetState() {
        this.currentPage = this.startingPageIndex
        this.previousTotalItemCount = 0
        this.loading = true
    }

    // Defines the process for actually loading more data based on page
    abstract fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView)

}
