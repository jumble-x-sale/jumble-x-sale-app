package com.jumble_x_sale.app.view.sale

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.util.formatGerman
import com.jumble_x_sale.app.view.LoggedInFragmentBase

class SoldItemFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: SoldItemViewModel
    private var itemId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        itemId = arguments?.getInt("itemId") ?: 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(SoldItemViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_sold_item, container, false)!!

        val item = app.ownCompletedSales.data.value?.get(itemId)

        root.findViewById<TextView>(R.id.category_text).apply { text = item?.category }
        root.findViewById<TextView>(R.id.title_text).apply { text = item?.title }
        root.findViewById<TextView>(R.id.description_text).apply { text = item?.description }
        root.findViewById<TextView>(R.id.price_text).apply { text = item?.price.formatGerman() }

        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.sold_item, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.list_messages -> navigate(SoldItemFragmentDirections.actionToCompletedSaleMessageList(itemId))
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        @JvmStatic private val TAG = SoldItemFragment::class.java.simpleName
    }

}
