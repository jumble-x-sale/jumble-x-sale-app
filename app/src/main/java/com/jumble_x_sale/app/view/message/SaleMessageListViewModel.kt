package com.jumble_x_sale.app.view.message

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.data.model.SaleOfferMessageData
import com.jumble_x_sale.app.data.model.SaleOfferMessageDataPage
import com.jumble_x_sale.app.data.repo.RepositoryBase
import com.jumble_x_sale.app.data.repo.SaleOfferMessageRepository
import com.jumble_x_sale.app.view.ListViewModelBase

class SaleMessageListViewModel(
    application: Application
) : ListViewModelBase<SaleOfferMessageData, SaleOfferMessageDataPage, SaleOfferMessageRepository>(application) {

    override var repo: SaleOfferMessageRepository = app.saleOfferMessages

    companion object {
        @JvmStatic private val TAG = SaleMessageListViewModel::class.java.simpleName
    }

}
