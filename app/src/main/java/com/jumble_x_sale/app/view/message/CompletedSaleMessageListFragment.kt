package com.jumble_x_sale.app.view.message

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.CompletedSaleMessageData
import com.jumble_x_sale.app.data.model.CompletedSaleMessageDataPage
import com.jumble_x_sale.app.data.repo.CompletedSaleMessageRepository
import com.jumble_x_sale.app.view.ListFragmentBase

class CompletedSaleMessageListFragment : ListFragmentBase<
    CompletedSaleMessageData,
    CompletedSaleMessageDataPage,
    CompletedSaleMessageRepository,
    CompletedSaleMessageListViewModel,
    CompletedSaleMessageListAdapter.ViewHolder,
    CompletedSaleMessageListAdapter
>() {

    override lateinit var viewModel: CompletedSaleMessageListViewModel
    private var soldItem: CompletedSaleData? = null
    private lateinit var menu: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.sale_message_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        /*
        when (item.itemId) {
            R.id.to_user_message_list -> navigate(CompletedSaleMessageListFragmentDirections.actionToUserMessageList())
        }
        */
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        soldItem = app.ownCompletedSales.data.value?.get(arguments?.getInt("sellItemId") ?: 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(CompletedSaleMessageListViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_sale_message_list, container, false)!!

        viewModel.repo.completedSale = soldItem

        return root
    }

    override fun getRecycler(): RecyclerView = root.findViewById(R.id.list_view_sale_message)

    override fun getAdapter() = CompletedSaleMessageListAdapter(viewModel)

    override fun getActionToItem(itemId: Int) = CompletedSaleMessageListFragmentDirections.actionToItem(itemId)

    companion object {
        @JvmStatic private val TAG = CompletedSaleMessageListFragment::class.java.simpleName
    }

}
