package com.jumble_x_sale.app.view.secret_santa

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.data.model.SecretSantaOfferDataPage
import com.jumble_x_sale.app.data.repo.TakeableSecretSantaOfferRepository
import com.jumble_x_sale.app.view.ListFragmentBase

class TakeableListFragment : ListFragmentBase<
        SecretSantaOfferData,
        SecretSantaOfferDataPage,
        TakeableSecretSantaOfferRepository,
        TakeableListViewModel,
        TakeableListAdapter.ViewHolder,
        TakeableListAdapter
>() {

    override lateinit var viewModel: TakeableListViewModel
    private lateinit var menu: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.takeable_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.to_givable_list -> navigate(TakeableListFragmentDirections.actionToGivableList())
            R.id.to_given_and_taken_list -> navigate(TakeableListFragmentDirections.actionToGivenAndTakenList())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(TakeableListViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_takeable_list, container, false)!!

        return root
    }

    override fun getRecycler(): RecyclerView = root.findViewById(R.id.list_view_takeable)

    override fun getAdapter() = TakeableListAdapter(viewModel)

    override fun getActionToItem(itemId: Int) = TakeableListFragmentDirections.actionToItem(itemId)

    companion object {
        @JvmStatic private val TAG = TakeableListFragment::class.java.simpleName
    }

}
