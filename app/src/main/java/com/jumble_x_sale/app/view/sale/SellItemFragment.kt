package com.jumble_x_sale.app.view.sale

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.util.encodeBase64
import com.jumble_x_sale.app.util.decodeBase64
import com.jumble_x_sale.app.util.scale
import com.jumble_x_sale.app.view.LoggedInFragmentBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SellItemFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: SellItemViewModel
    private var itemId: Int = 0
    private var ownSellItem: SaleOfferData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        itemId = arguments?.getInt("itemId") ?: 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(SellItemViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_sell_item, container, false)!!

        ownSellItem = app.ownSaleOffers.data.value?.get(itemId)

        bindEditText(root, R.id.category_text, viewModel.category)
        bindEditText(root, R.id.title_text, viewModel.title)
        bindEditText(root, R.id.description_text, viewModel.description)
        bindEditNumber(root, R.id.price_text, viewModel.price)
        bindEditImage(root, R.id.photo_image, viewModel.photo) { it?.scale(200) }

        viewModel.category.value = ownSellItem?.category
        viewModel.title.value = ownSellItem?.title
        viewModel.description.value = ownSellItem?.description
        viewModel.price.value = ownSellItem?.price
        viewModel.photo.value = ownSellItem?.mainImageData.decodeBase64()

        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.sell_item, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.list_messages -> navigate(SellItemFragmentDirections.actionToSaleMessageList(itemId))
            R.id.change_item -> change()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun change() {
        if (viewModel.category.value == null) {
            Toast.makeText(app, R.string.category_missing, Toast.LENGTH_LONG).show()
        } else if (viewModel.title.value == null) {
            Toast.makeText(app, R.string.title_missing, Toast.LENGTH_LONG).show()
        } else if (viewModel.description.value == null) {
            Toast.makeText(app, R.string.description_missing, Toast.LENGTH_LONG).show()
        } else if (viewModel.price.value == null) {
            Toast.makeText(app, R.string.price_missing, Toast.LENGTH_LONG).show()
        } else {
            val data = SaleOfferData(
                id = ownSellItem?.id,
                category = viewModel.category.value,
                title = viewModel.title.value,
                description = viewModel.description.value,
                price = viewModel.price.value,
                mainImageData = viewModel.photo.value.encodeBase64())

            app.api.saleOfferUpdate(data).enqueue(object : Callback<Unit> {
                override fun onResponse(call: Call<Unit>?, response: Response<Unit>?) {
                    if (response != null && response.isSuccessful && response.body() != null) {
                        changeResult(data)
                    } else {
                        Log.w(TAG, response?.toString() ?: "")
                        changeResult()
                    }
                }

                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    Log.w(TAG, t.toString())
                    changeResult()
                }
            })
        }
    }

    private fun changeResult(data: SaleOfferData? = null) {
        if (data != null) {
            ownSellItem?.category = data.category
            ownSellItem?.title = data.title
            ownSellItem?.description = data.description
            ownSellItem?.price = data.price
            Toast.makeText(app, R.string.sale_offer_changed, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(app, R.string.network_error, Toast.LENGTH_LONG).show()
        }
        navigateUp()
    }

    companion object {
        @JvmStatic private val TAG = SellItemFragment::class.java.simpleName
    }

}
