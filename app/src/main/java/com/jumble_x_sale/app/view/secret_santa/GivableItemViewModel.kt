package com.jumble_x_sale.app.view.secret_santa

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import java.time.Instant
import java.util.UUID

class GivableItemViewModel(application: Application) : AndroidViewModel(application) {

    val creatorId = MutableLiveData<UUID>()
    val creationInstant = MutableLiveData<Instant>()
    val category = MutableLiveData<String>()
    val title = MutableLiveData<String>()
    val description = MutableLiveData<String>()
    val mainImageData = MutableLiveData<ByteArray>()

}
