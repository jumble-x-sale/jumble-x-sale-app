package com.jumble_x_sale.app.view.account

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.Credentials
import com.jumble_x_sale.app.data.model.Session
import com.jumble_x_sale.app.view.FragmentBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginFragment : FragmentBase() {

    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_login, container, false)!!

        bindEditText(root, R.id.email_text, viewModel.email)
        bindEditText(root, R.id.password_text, viewModel.password)

        if (viewModel.email.value == null) { viewModel.email.value = "max@mustermann.de" }
        if (viewModel.password.value == null) { viewModel.password.value = "1234" }

        root.findViewById<Button>(R.id.login_btn).setOnClickListener { login() }
        root.findViewById<Button>(R.id.register_btn).setOnClickListener { register() }

        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.login, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.login_done -> { login() }
            R.id.action_to_register -> { navigate(LoginFragmentDirections.actionToRegister()) }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun login() {
        hideKeyboard()

        when {
            viewModel.email.value == null -> {
                Toast.makeText(app, R.string.email_missing, Toast.LENGTH_LONG).show()
            }
            viewModel.password.value == null -> {
                Toast.makeText(app, R.string.password_missing, Toast.LENGTH_LONG).show()
            }
            else -> {
                val credentials = Credentials(viewModel.email.value!!, viewModel.password.value!!)

                navigate(LoginFragmentDirections.actionToWait())

                app.api.userLogin(credentials).enqueue(object : Callback<Session> {
                    override fun onResponse(call: Call<Session>?, response: Response<Session>?) {
                        if (response != null && response.isSuccessful && response.body() != null) {
                            app.setSession(
                                email = viewModel.email.value,
                                password = viewModel.password.value,
                                session = response.body()
                            )
                            mainActivity.updateEmail()
                            navigate(LoginFragmentDirections.actionToHome())
                        } else {
                            Log.w(TAG, response?.toString() ?: "")
                            loginError(response?.code() == 403)
                        }
                    }

                    override fun onFailure(call: Call<Session>?, t: Throwable?) {
                        Log.w(TAG, t.toString())
                        loginError()
                    }
                })
            }
        }
    }

    private fun loginError(forbidden: Boolean = false) {
        navigate(LoginFragmentDirections.actionToLogin())
        val text = if (forbidden) {
                getString(R.string.forbidden)
            } else {
                getString(R.string.network_error)
            }
        val duration = Toast.LENGTH_LONG
        val toast = Toast.makeText(app, text, duration)
        toast.show()
    }

    private fun register() {
        hideKeyboard()
        navigate(LoginFragmentDirections.actionToRegister())
    }

    companion object {
        @JvmStatic private val TAG = LoginFragment::class.java.simpleName
    }

}
