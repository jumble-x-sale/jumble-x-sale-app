package com.jumble_x_sale.app.view.account

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.Credentials
import com.jumble_x_sale.app.data.model.Session
import com.jumble_x_sale.app.data.model.UserData
import com.jumble_x_sale.app.view.FragmentBase
import java.util.UUID
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterFragment : FragmentBase() {

    private lateinit var viewModel: RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_register, container, false)!!

        this.bindEditText(root, R.id.firstName_text, viewModel.firstName)
        this.bindEditText(root, R.id.lastName_text, viewModel.lastName)
        this.bindEditText(root, R.id.email_text, viewModel.email)
        this.bindEditText(root, R.id.password_text, viewModel.password)
        this.bindEditText(root, R.id.id_card_text, viewModel.idCard)

        if (viewModel.firstName.value == null) { viewModel.firstName.value = "Max" }
        if (viewModel.lastName.value == null) { viewModel.lastName.value = "Mustermann" }
        if (viewModel.email.value == null) { viewModel.email.value = "max@mustermann.de" }
        if (viewModel.password.value == null) { viewModel.password.value = "1234" }
        if (viewModel.idCard.value == null) { viewModel.idCard.value = "lhm310k1g398092282302096" }

        root.findViewById<Button>(R.id.register_btn).setOnClickListener { register() }
        root.findViewById<Button>(R.id.login_btn).setOnClickListener { navigate(LoginFragmentDirections.actionToLogin()) }

        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.register, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.register_done -> { register() }
            R.id.action_to_login -> { navigate(LoginFragmentDirections.actionToLogin()) }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun register() {
        hideKeyboard()

        when {
            viewModel.firstName.value == null -> {
                Toast.makeText(app, R.string.firstname_missing, Toast.LENGTH_LONG).show()
            }
            viewModel.lastName.value == null -> {
                Toast.makeText(app, R.string.lastname_missing, Toast.LENGTH_LONG).show()
            }
            viewModel.idCard.value == null -> {
                Toast.makeText(app, R.string.id_card_missing, Toast.LENGTH_LONG).show()
            }
            viewModel.email.value == null -> {
                Toast.makeText(app, R.string.email_missing, Toast.LENGTH_LONG).show()
            }
            viewModel.password.value == null -> {
                Toast.makeText(app, R.string.password_missing, Toast.LENGTH_LONG).show()
            }
            else -> {
                val credentials = Credentials(viewModel.email.value!!, viewModel.password.value!!)
                val userData = UserData(
                    firstName = viewModel.firstName.value,
                    lastName = viewModel.lastName.value,
                    password = viewModel.password.value,
                    email = viewModel.email.value,
                    idCard = viewModel.idCard.value)

                app.api.userCreate(userData).enqueue(object : Callback<UUID> {
                    override fun onResponse(call: Call<UUID>?, response: Response<UUID>?) {
                        if (response != null && response.isSuccessful && response.body() != null) {
                            app.api.userLogin(credentials).enqueue(object : Callback<Session> {
                                override fun onResponse(call: Call<Session>?, response: Response<Session>?) {
                                    if (response != null && response.isSuccessful && response.body() != null) {
                                        app.setSession(
                                            email = viewModel.email.value,
                                            password = viewModel.password.value,
                                            session = response.body())
                                        mainActivity.updateEmail()
                                        navigate(RegisterFragmentDirections.actionToHome())
                                    } else {
                                        Log.w(TAG, response?.toString() ?: "")
                                    }
                                }

                                override fun onFailure(call: Call<Session>?, t: Throwable?) {
                                    Log.w(TAG, t.toString())
                                }
                            })
                        } else {
                            Log.w(TAG, response?.toString() ?: "")
                        }
                    }

                    override fun onFailure(call: Call<UUID>, t: Throwable) {
                        Log.w(TAG, t.toString())
                    }
                })
            }
        }
    }

    companion object {
        @JvmStatic private val TAG = RegisterFragment::class.java.simpleName
    }

}
