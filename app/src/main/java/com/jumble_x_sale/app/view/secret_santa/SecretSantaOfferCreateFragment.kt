package com.jumble_x_sale.app.view.secret_santa

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.view.LoggedInFragmentBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.Instant
import java.util.UUID

class SecretSantaOfferCreateFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: SecretSantaOfferCreateViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(SecretSantaOfferCreateViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_secret_santa_offer_create, container, false)!!

        bindEditText(root, R.id.category_text, viewModel.category)
        bindEditText(root, R.id.title_text, viewModel.title)
        bindEditText(root, R.id.description_text, viewModel.description)

        // root.findViewById<Button>(R.id.create_btn).setOnClickListener { complete() }

        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.secret_santa_offer_create, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.done -> complete()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun complete() {
        if (viewModel.category.value == null) {
            Toast.makeText(app, R.string.category_missing, Toast.LENGTH_LONG).show()
        } else if (viewModel.title.value == null) {
            Toast.makeText(app, R.string.title_missing, Toast.LENGTH_LONG).show()
        } else if (viewModel.description.value == null) {
            Toast.makeText(app, R.string.description_missing, Toast.LENGTH_LONG).show()
        } else {
            val data = SecretSantaOfferData(
                creatorId = app.userId,
                creationInstant = Instant.now(),
                category = viewModel.category.value,
                title = viewModel.title.value,
                description = viewModel.description.value)

            app.api.secretSantaOfferCreate(data).enqueue(object : Callback<UUID> {
                override fun onResponse(call: Call<UUID>?, response: Response<UUID>?) {
                    if (response != null && response.isSuccessful && response.body() != null) {
                        completeResult(data)
                    } else {
                        Log.w(TAG, response?.toString() ?: "")
                        completeResult()
                    }
                }

                override fun onFailure(call: Call<UUID>, t: Throwable) {
                    Log.w(TAG, t.toString())
                    completeResult()
                }
            })
        }
    }

    private fun completeResult(data: SecretSantaOfferData? = null) {
        if (data != null) {
            app.givableSecretSantaOffers.data.value?.add(data)
            Toast.makeText(app, R.string.sale_offer_created, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(app, R.string.network_error, Toast.LENGTH_LONG).show()
        }
        navigateUp()
    }

    companion object {
        @JvmStatic private val TAG = SecretSantaOfferCreateFragment::class.java.simpleName
    }

}
