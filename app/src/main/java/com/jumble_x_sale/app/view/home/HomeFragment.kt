package com.jumble_x_sale.app.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.view.LoggedInFragmentBase

class HomeFragment : LoggedInFragmentBase() {

    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_home, container, false)!!

        root.findViewById<Button>(R.id.purchase_btn).setOnClickListener { navigate(HomeFragmentDirections.actionToPurchaseList()) }
        root.findViewById<Button>(R.id.sell_btn).setOnClickListener { navigate(HomeFragmentDirections.actionToSellList()) }
        root.findViewById<Button>(R.id.takeable_btn).setOnClickListener { navigate(HomeFragmentDirections.actionToTakeableList()) }
        root.findViewById<Button>(R.id.givable_btn).setOnClickListener { navigate(HomeFragmentDirections.actionToGivableList()) }
        root.findViewById<Button>(R.id.user_messages_btn).setOnClickListener { navigate(HomeFragmentDirections.actionToUserMessageList()) }
        // root.findViewById<Button>(R.id.sale_messages_btn).setOnClickListener { navigate(HomeFragmentDirections.actionToSaleMessageList()) }
        root.findViewById<Button>(R.id.account_btn).setOnClickListener { navigate(HomeFragmentDirections.actionToAccount()) }

        return root
    }

    companion object {
        @JvmStatic private val TAG = HomeFragment::class.java.simpleName
    }

}
