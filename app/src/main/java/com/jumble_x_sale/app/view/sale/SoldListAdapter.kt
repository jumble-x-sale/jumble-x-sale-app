package com.jumble_x_sale.app.view.sale

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jumble_x_sale.app.R
import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.CompletedSaleDataPage
import com.jumble_x_sale.app.data.repo.OwnCompletedSaleRepository
import com.jumble_x_sale.app.util.Utils
import com.jumble_x_sale.app.util.formatGerman
import com.jumble_x_sale.app.view.ListAdapterBase
import java.util.Locale

class SoldListAdapter(
    viewModel: SoldListViewModel
) : ListAdapterBase<
        CompletedSaleData,
        CompletedSaleDataPage,
        OwnCompletedSaleRepository,
        SoldListViewModel,
        SoldListAdapter.ViewHolder
>(viewModel) {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val titleView: TextView = itemView.findViewById(R.id.title_text)
        val priceView: TextView = itemView.findViewById(R.id.price_text)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.list_item_sold, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        if (data != null) {
            val item = data?.get(i)!!
            viewHolder.titleView.text = item.title
            viewHolder.priceView.text = item.price.formatGerman()

            viewHolder.itemView.setOnClickListener { _ -> viewModel.setItemSelected(i) }
        }
    }

}
