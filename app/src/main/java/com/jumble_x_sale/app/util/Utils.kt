package com.jumble_x_sale.app.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Base64
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.math.BigDecimal
import java.nio.ByteBuffer
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.text.ParseException
import java.util.Locale
import java.util.UUID
import kotlin.math.min

class Utils {

    companion object {
        @JvmStatic internal val TAG = Utils::class.simpleName
        @JvmStatic private val decimalFormatSymbols = DecimalFormatSymbols()
        @JvmStatic internal val decimalFormat: DecimalFormat
        @JvmStatic private val germanDecimalFormatSymbols = DecimalFormatSymbols()
        @JvmStatic internal val germanDecimalFormat: DecimalFormat

        init {
            decimalFormatSymbols.groupingSeparator = ','
            decimalFormatSymbols.decimalSeparator = '.'
            decimalFormat = DecimalFormat("#,##0.0#", decimalFormatSymbols)
            decimalFormat.isParseBigDecimal = true
            germanDecimalFormatSymbols.groupingSeparator = '.'
            germanDecimalFormatSymbols.decimalSeparator = ','
            // germanDecimalFormat = NumberFormat.getInstance(Locale.GERMAN) as DecimalFormat
            germanDecimalFormat = DecimalFormat("#,##0.00", germanDecimalFormatSymbols)
            germanDecimalFormat.isParseBigDecimal = true
        }

        /**
         * Try to get an enum value from an id string.
         * 
         * @param <E>          The type of enum to get.
         * @param id           The id string to get an enum value from.
         * @param defaultValue The default enum value.
         * @return An enum value from an id string.
         */
        inline fun <reified E : Enum<E>> tryValueOf(id: String?, defaultValue: E?): E?  {
            if (id != null) {
                return try {
                    @Suppress("UPPER_BOUND_VIOLATED")
                    enumValueOf<E>(id)
                } catch (e: IllegalArgumentException) {
                    defaultValue
                }
            }
            return defaultValue
        }

        /**
         * Divide a long value and ceil the result.
         * 
         * @param dividend The dividend.
         * @param divisor  The divisor.
         * @return The quotient.
         */
        fun divideCeil(dividend: Long, divisor: Long): Long {
            if (divisor != 0L) {
                return dividend / divisor +
                    if (dividend % divisor == 0L) {
                        0
                    } else {
                        1
                    }
            }
            return 0
        }
    }

}

/**
 * Creates a [UUID] from the string standard representation as described in the
 * [#toString] method.
 *
 * @return A [UUID] with the specified value or null when that fails.
 */
fun String?.toUuid(): UUID? {
    return if (this == null) {
        null
    } else {
        try {
            UUID.fromString(this)
        } catch (e: IllegalArgumentException) {
            null
        }
    }
}

/**
 * Parse a [BigDecimal].
 *
 * @return A [BigDecimal].
 */
fun String?.parseBigDecimal(): BigDecimal? =
    if (this == null) {
        null
    } else {
        try {
            Utils.decimalFormat.parse(this) as BigDecimal
        } catch (pe: ParseException) {
            null
        }
    }

/**
 * Parse a [BigDecimal] using german locale.
 *
 * @return A [BigDecimal] using german locale.
 */
fun String?.parseBigDecimalGerman(): BigDecimal? =
    if (this == null) {
        null
    } else {
        try {
            Utils.germanDecimalFormat.parse(this) as BigDecimal
        } catch (pe: ParseException) {
            null
        }
    }

/**
 * Format a [BigDecimal] as [String].
 *
 * @return A [BigDecimal] as [String].
 */
fun BigDecimal?.format(): String? =
    if (this == null) {
        null
    } else {
        Utils.decimalFormat.format(this)
    }

/**
 * Format a [BigDecimal] as [String] using german locale.
 *
 * @return A [BigDecimal] as [String] using german locale.
 */
fun BigDecimal?.formatGerman(): String? =
    if (this == null) {
        null
    } else {
        Utils.germanDecimalFormat.format(this)
    }

/**
 * Converts a byte array to a bitmap
 *
 * @return The resulting bitmap.
 */
fun ByteArray.convertCompressedByteArrayToBitmap(): Bitmap = BitmapFactory.decodeByteArray(this, 0, size)

/**
 * Converts bitmap to byte array givable a compression format.
 *
 * @param format  The compression format to apply.
 * @param quality The compression quality to apply.
 * @return The resulting byte array.
 */
fun Bitmap.toByteArray(
    format: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG,
    quality: Int = 0
): ByteArray {
    var stream: ByteArrayOutputStream? = null
    return try {
        stream = ByteArrayOutputStream()
        compress(format, quality, stream)
        stream.toByteArray()
    } finally {
        if (stream != null) {
            try {
                stream.close()
            } catch (e: IOException) {
                Log.e(Utils.TAG, "ByteArrayOutputStream was not closed")
            }
        }
    }
}

/**
 * Converts bitmap to byte array without compression.
 *
 * @return The resulting byte array.
 */
fun Bitmap.toByteArrayUncompressed(): ByteArray = ByteBuffer.allocate(byteCount).apply {
        copyPixelsToBuffer(this)
        rewind()
    }.array()

/**
 * Scales a bitmap to fit in the givable maximum dimension.
 *
 * @param  maxWidthOrHeight The maximum width or height for the bitmap.
 * @return The resulting bitmap.
 */
fun Bitmap.scale(maxWidthOrHeight: Int): Bitmap {
    val factor = min(
        maxWidthOrHeight.toDouble() / width.toDouble(),
        maxWidthOrHeight.toDouble() / height.toDouble()).toFloat()
    return Bitmap.createScaledBitmap(
            this,
            (width.toFloat() * factor).toInt(),
            (height.toFloat() * factor).toInt(),
            true)
}

/**
 * Scales a bitmap to fit in the givable maximum dimension.
 *
 * @param  maxWidth  The maximum width for the bitmap.
 * @param  maxHeight The maximum height for the bitmap.
 * @return The resulting bitmap.
 */
fun Bitmap.scale(maxWidth: Int, maxHeight: Int): Bitmap {
    val factor = min(
        maxWidth.toDouble() / width.toDouble(),
        maxHeight.toDouble() / height.toDouble()).toFloat()
    return Bitmap.createScaledBitmap(
            this,
            (width.toFloat() * factor).toInt(),
            (height.toFloat() * factor).toInt(),
            true)
}

/**
 * Centers a bitmap and crops the sides of the greater dimension.
 *
 * @return The resulting bitmap.
 */
fun Bitmap.centerCrop(): Bitmap = when {
        width >= height -> {
            Bitmap.createBitmap(this, (width / 2) - (height / 2), 0, height, height)
        }
        width <= height -> {
            Bitmap.createBitmap(this, 0, (height / 2) - (width / 2), width, width)
        }
        else -> {
            this
        }
    }

fun String?.decodeBase64(): Bitmap? =
    if (this == null) {
        null
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        java.util.Base64.getDecoder().decode(this).convertCompressedByteArrayToBitmap()
    } else {
        // TODO: The API-server has problems with this solution.
        Base64.decode(this, Base64.DEFAULT).convertCompressedByteArrayToBitmap()
    }

fun Bitmap?.encodeBase64(): String? =
    if (this == null) {
        null
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        java.util.Base64.getEncoder().encodeToString(this.toByteArray())
    } else {
        // TODO: The API-server has problems with this solution.
        Base64.encodeToString(this.toByteArray(), Base64.DEFAULT)
    }
