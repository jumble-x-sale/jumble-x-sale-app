@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.util

import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.zone.ZoneRules
import java.util.Date
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.DateTimeException
import java.time.Instant
import java.time.LocalDate

/**
 * Helper for time related data types.
 */
class TimeUtils private constructor() {

    companion object {
        /** The formatter for a UTC date. */
        @JvmStatic val UTC_DATE: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'Z'")

        /** The formatter for a UTC date time with milliseconds. */
        @JvmStatic val UTC_DATE_TIME: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")

        /** The formatter for a UTC time with milliseconds. */
        @JvmStatic val UTC_TIME: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS'Z'")

        /** The formatter for a local date. */
        @JvmStatic val LOCAL_DATE: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

        /** The formatter for a local date time with milliseconds. */
        @JvmStatic val LOCAL_DATE_TIME: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")

        /** The formatter for a local time with milliseconds. */
        @JvmStatic val LOCAL_TIME: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS")

        /** The time zone id of germany. */
        @JvmStatic val ZoneId_Germany: ZoneId = ZoneId.of("Europe/Berlin")

        /** The time zone rules for germany. */
        @JvmStatic val ZoneRules_Germany: ZoneRules = ZoneId_Germany.rules

        /** Today as UTC date. */
        fun todayUtc(): LocalDate = Instant.now().toUtcDate()

        /** Now as UTC date time. */
        fun nowUtc(): LocalDateTime = Instant.now().toUtcDateTime()

        /** Today as german date. */
        fun todayGerman(): LocalDate = nowGerman().toLocalDate()

        /** Now as german date time. */
        fun nowGerman(): LocalDateTime = LocalDateTime.ofInstant(Instant.now(), ZoneId_Germany)

        /**
         * Obtains an instance of [OffsetDateTime] from year, month, day, hour,
         * minute, second and nanosecond.
         * <p>
         * This returns a [OffsetDateTime] with the specified year, month,
         * day-of-month, hour, minute, second and nanosecond. The day must be valid for
         * the year and month, otherwise an exception will be thrown.
         *
         * @param year         the year to represent, from MIN_YEAR to MAX_YEAR
         * @param month        the month-of-year to represent, from 1 (January) to 12
         *                     (December)
         * @param dayOfMonth   the day-of-month to represent, from 1 to 31
         * @param hour         the hour-of-day to represent, from 0 to 23
         * @param minute       the minute-of-hour to represent, from 0 to 59
         * @param second       the second-of-minute to represent, from 0 to 59
         * @param nanoOfSecond the nano-of-second to represent, from 0 to 999,999,999
         * @param zoneId       the zone id to use
         * @return the offset date-time, not null
         * @throws DateTimeException if the value of any field is out of range, or if
         *                           the day-of-month is invalid for the month-year
         */
        fun of(
            year: Int,
            month: Int,
            dayOfMonth: Int,
            hour: Int = 0,
            minute: Int = 0,
            second: Int = 0,
            nanoOfSecond: Int = 0,
            zoneId: ZoneId = ZoneOffset.systemDefault()
        ): OffsetDateTime =
            LocalDateTime.of(year, month, dayOfMonth, hour, minute, second, nanoOfSecond).toOffsetDateTime(zoneId)

        /**
         * Obtains an instance of [OffsetDateTime] from year, month, day, hour,
         * minute, second and nanosecond.
         * <p>
         * This returns a [OffsetDateTime] with the specified year, month,
         * day-of-month, hour, minute, second and nanosecond. The day must be valid for
         * the year and month, otherwise an exception will be thrown.
         *
         * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
         * @param month      the month-of-year to represent, from 1 (January) to 12
         *                   (December)
         * @param dayOfMonth the day-of-month to represent, from 1 to 31
         * @param hour       the hour-of-day to represent, from 0 to 23
         * @param minute     the minute-of-hour to represent, from 0 to 59
         * @param second     the second-of-minute to represent, from 0 to 59
         * @return the offset date-time, not null
         * @throws DateTimeException if the value of any field is out of range, or if
         *                           the day-of-month is invalid for the month-year
         */
        fun utcInstantOf(
            year: Int,
            month: Int,
            dayOfMonth: Int,
            hour: Int = 0,
            minute: Int = 0,
            second: Int = 0
        ): Instant =
            LocalDateTime.of(year, month, dayOfMonth, hour, minute, second).toInstant(ZoneOffset.UTC)

        /**
         * Obtains an instance of [OffsetDateTime] from year, month, day, hour,
         * minute, second and nanosecond.
         * <p>
         * This returns a [OffsetDateTime] with the specified year, month,
         * day-of-month, hour, minute, second and nanosecond. The day must be valid for
         * the year and month, otherwise an exception will be thrown.
         *
         * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
         * @param month      the month-of-year to represent, from 1 (January) to 12
         *                   (December)
         * @param dayOfMonth the day-of-month to represent, from 1 to 31
         * @param hour       the hour-of-day to represent, from 0 to 23
         * @param minute     the minute-of-hour to represent, from 0 to 59
         * @param second     the second-of-minute to represent, from 0 to 59
         * @return the offset date-time, not null
         * @throws DateTimeException if the value of any field is out of range, or if
         *                           the day-of-month is invalid for the month-year
         */
        fun germanInstantOf(
            year: Int,
            month: Int,
            dayOfMonth: Int,
            hour: Int = 0,
            minute: Int = 0,
            second: Int = 0
        ): Instant =
            LocalDateTime.of(year, month, dayOfMonth, hour, minute, second).asGermanInstant()
    }
}

/**
 * Convert an [Instant] to a german [LocalDate].
 */
fun Instant.toGermanDate(): LocalDate = toGermanDateTime().toLocalDate()

/**
 * Convert an [Instant] to a german [LocalDateTime].
 */
fun Instant.toGermanDateTime(): LocalDateTime =
    LocalDateTime.ofInstant(this, TimeUtils.ZoneRules_Germany.getOffset(this))

/**
 * Convert an [Instant] to a UTC [LocalDate].
 */
fun Instant.toUtcDate(): LocalDate = toUtcDateTime().toLocalDate()

/**
 * Convert an [Instant] to a [LocalDateTime].
 */
fun Instant.toUtcDateTime(): LocalDateTime =
    LocalDateTime.ofInstant(this, ZoneOffset.UTC)

/**
 * Convert a UTC [LocalDate] to an [Instant].
 */
fun LocalDate.toInstant(zoneId: ZoneId = ZoneOffset.systemDefault()): Instant =
    atStartOfDay().toInstant(zoneId)

/**
 * Convert a [LocalDateTime] to an [Instant].
 */
fun LocalDateTime.toInstant(zoneId: ZoneId = ZoneOffset.systemDefault()): Instant =
    toInstant(zoneId.rules.getOffset(this))

/**
 * Convert a UTC date to an [Instant].
 */
fun LocalDate.asGermanInstant(): Instant = atStartOfDay().asGermanInstant()

/**
 * Convert a german local date time to an [Instant].
 */
fun LocalDateTime.asGermanInstant(): Instant =
    toInstant(TimeUtils.ZoneRules_Germany.getOffset(this))

/**
 * Convert a UTC [LocalDate] to an [Instant].
 */
fun LocalDate.asUtcInstant(): Instant = atStartOfDay().toInstant(ZoneOffset.UTC)

/**
 * Convert a UTC [LocalDateTime] to an [Instant].
 *
 * @return A [Instant] from a UTC [LocalDateTime].
 */
fun LocalDateTime.asUtcInstant(): Instant = toInstant(ZoneOffset.UTC)

/**
 * Get an [OffsetDateTime] from a [LocalDateTime] and a [ZoneId]
 *
 * @param zoneId The [ZoneId] to use.
 * @return The resulting [OffsetDateTime].
 */
fun LocalDateTime.toOffsetDateTime(zoneId: ZoneId = ZoneOffset.systemDefault()): OffsetDateTime =
    OffsetDateTime.of(this, zoneId.rules.getOffset(this))

/**
 * Get a [Date] from a [LocalDateTime].
 *
 * @param zoneId The [ZoneId] to use.
 * @return A [Date] from a [LocalDateTime].
 */
fun LocalDateTime.toDate(zoneId: ZoneId = ZoneOffset.systemDefault()): Date =
    Date.from(this.atZone(zoneId).toInstant())

/**
 * Get a [LocalDateTime] from a [Date].
 *
 * @param zoneId The [ZoneId] to use.
 * @return A [LocalDateTime] from a [Date].
 */
fun Date.toLocalDateTime(zoneId: ZoneId = ZoneOffset.systemDefault()): LocalDateTime =
    LocalDateTime.ofInstant(this.toInstant(), zoneId)

/**
 * Format an [Instant] as text.
 * 
 * @param formatter The formatter to use.
 * @return An [Instant] as text.
 */
fun Instant.format(formatter: DateTimeFormatter = TimeUtils.UTC_DATE_TIME): String =
    formatter.format(LocalDateTime.ofEpochSecond(epochSecond, nano, ZoneOffset.UTC))

/**
 * Format an [LocalDate] as text.
 * 
 * @return An [LocalDate] as text.
 */
fun LocalDate.format(): String = TimeUtils.LOCAL_DATE.format(this)

/**
 * Format an [LocalDateTime] as text.
 * 
 * @return An [LocalDateTime] as text.
 */
fun LocalDateTime.format(): String = TimeUtils.LOCAL_DATE.format(this)

/**
 * Format an [LocalTime] as text.
 * 
 * @return An [LocalTime] as text.
 */
fun LocalTime.format(): String = TimeUtils.LOCAL_TIME.format(this)

/**
 * Parse an [Instant] from text.
 * 
 * @return an [Instant] from text.
 */
fun String?.parseInstant(): Instant =
    LocalDateTime.parse(this, TimeUtils.UTC_DATE_TIME).toInstant(ZoneOffset.UTC)

/**
 * Parse a [LocalDate LocalDate] from text.
 * 
 * @return an [LocalDate LocalDate] from text.
 */
fun String?.parseLocalDate(): LocalDate = LocalDate.parse(this, TimeUtils.LOCAL_DATE)

/**
 * Parse a [LocalDateTime] from text.
 * 
 * @return an [LocalDateTime] from text.
 */
fun String?.parseLocalDateTime(): LocalDateTime =
    LocalDateTime.parse(this, TimeUtils.LOCAL_DATE_TIME)

/**
 * Parse a [LocalTime] from text.
 * 
 * @return an [LocalTime] from text.
 */
fun String?.parseLocalTime(): LocalTime = LocalTime.parse(this, TimeUtils.LOCAL_TIME)
