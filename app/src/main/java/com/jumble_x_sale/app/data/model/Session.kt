package com.jumble_x_sale.app.data.model

import com.squareup.moshi.JsonClass
import java.util.UUID

@JsonClass(generateAdapter = true)
data class Session(
    var userId: UUID,
    var accessToken: String)
