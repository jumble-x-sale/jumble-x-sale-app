@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.network.adapter

import com.jumble_x_sale.app.util.format
import com.jumble_x_sale.app.util.parseInstant
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.Instant

class InstantJsonAdapter {

    @FromJson
    fun fromJson(json: String): Instant {
        return json.parseInstant()
    }

    @ToJson
    fun toJson(value: Instant): String {
        return value.format()
    }

}
