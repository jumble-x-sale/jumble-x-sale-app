@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.network.adapter

import com.jumble_x_sale.app.util.format
import com.jumble_x_sale.app.util.parseBigDecimal
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.math.BigDecimal

class BigDecimalJsonAdapter {

    @FromJson
    fun fromJson(json: String): BigDecimal = json.parseBigDecimal()!!

    @ToJson
    fun toJson(value: BigDecimal): String = value.format()!!

}
