@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.model.util

import com.jumble_x_sale.app.util.parseBigDecimal
import com.jumble_x_sale.app.util.parseInstant
import com.jumble_x_sale.app.util.parseLocalDate
import com.jumble_x_sale.app.util.parseLocalDateTime
import com.jumble_x_sale.app.util.parseLocalTime
import com.jumble_x_sale.app.util.toUuid
import com.jumble_x_sale.app.util.Utils
import com.squareup.moshi.JsonClass
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.Locale
import java.util.UUID
import kotlin.collections.HashMap

@JsonClass(generateAdapter = true)
class MultiValueFilter internal constructor(
    propertyName: String?,
    propertyType: DataType?,
    var operator: Operator?,
    var values: Array<String?>?
) : Filter(propertyName, propertyType) {

    /*
    init {
        Array<String>
    }
    */

    /**
     * An operator that takes zero or more operands.
     */
    enum class Operator(val code: String) {
        /**
         * Equals a value in the list.
         */
        IN(IN_CODE),
        /**
         * Not equals a value in the list.
         */
        NOT_IN(NOT_IN_CODE);

        companion object {
            @JvmStatic private val valuesByCode = HashMap<String, Operator>()

            init {
                val types: Array<Operator> = values()
                for (i in types.indices) {
                    valuesByCode[types[i].code] = types[i]
                }
            }

            fun tryValueOf(idOrCode: String, defaultValue: Operator?): Operator? {
                val idOrCodeUpper = idOrCode.toUpperCase(Locale.ROOT)
                var value = valuesByCode[idOrCodeUpper]
                if (value == null) {
                    @Suppress("UPPER_BOUND_VIOLATED")
                    value = Utils.tryValueOf(idOrCodeUpper, defaultValue)
                }
                return value
            }

            fun tryValueOf(idOrCode: String): Operator? = tryValueOf(idOrCode, null)
        }
    }

    constructor() : this(null, null, null, null)

    fun valuesAsUUID(): List<UUID?>? = this.values?.map { it.toUuid() }
    fun valuesAsDecimal(): List<BigDecimal?>? = this.values?.map { it.parseBigDecimal() }
    fun valuesAsInstant(): List<Instant?>? = this.values?.map { it.parseInstant() }
    fun valuesAsDate(): List<LocalDate?>? = this.values?.map { it.parseLocalDate() }
    fun valuesAsDateTime(): List<LocalDateTime?>? = this.values?.map { it.parseLocalDateTime() }
    fun valuesAsTime(): List<LocalTime?>? = this.values?.map { it.parseLocalTime() }
    fun values(): List<String?>? = this.values?.toList()

    companion object {
        private const val IN_CODE = "IN"
        private const val NOT_IN_CODE = "NIN"
        private const val PN_VALUES = "values"
    }
}
