package com.jumble_x_sale.app.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.CompletedSaleDataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal

/**
 * Repository for completed sales from this member.
 */
class OwnCompletedSaleRepository(
    app: JumbleApplication
) : RepositoryBase<CompletedSaleData, CompletedSaleDataPage>(app) {

    override fun onRefresh(data: MutableList<CompletedSaleData>) =
        if (JumbleApplication.isDebugLocal) {
            data.apply {
                add(CompletedSaleData(title = "Lego", category = "Spielzeug", description = "Tut weh wenn man drauftritt", price = BigDecimal.valueOf(6.00)))
                add(CompletedSaleData(title = "CoD", category = "Videospiel", description = "ungenutzter Key", price = BigDecimal.valueOf(6.66)))
                add(CompletedSaleData(title = "Die Millionärsformel", category = "Buch", description = "neu verpackt", price = BigDecimal.valueOf(1.00)))
                add(CompletedSaleData(title = "LG 55 Zoll", category = "Elektronik", description = "7 Tage genutzt", price = BigDecimal.valueOf(2.00)))
                add(CompletedSaleData(title = "Bagger", category = "Schwergeräte", description = "dreckige Schaufel", price = BigDecimal.valueOf(3.00)))
                add(CompletedSaleData(title = "Mauspad", category = "Zubehör", description = "mit LED Rahmen", price = BigDecimal.valueOf(5.00)))
                add(CompletedSaleData(title = "WoW", category = "Videospiel", description = "ungenutzter Key", price = BigDecimal.valueOf(13.49)))
                add(CompletedSaleData(title = "Phasmophobia", category = "Videospiel", description = "Gruselt mich zu sehr", price = BigDecimal.valueOf(16.00)))
                add(CompletedSaleData(title = "Kinogutschein", category = "Gutscheine", description = "Läuft bald aus", price = BigDecimal.valueOf(8.00)))
                add(CompletedSaleData(title = "Lego Mindstorm", category = "Elektronik", description = "Ist mir zu kompliziert & Programm inklusive", price = BigDecimal.valueOf(7.00)))
                add(CompletedSaleData(title = "Ryzen 7 3700X", category = "Elektronik", description = "Super schneller Prozessor", price = BigDecimal.valueOf(9.00)))
                add(CompletedSaleData(title = "OnePlus 8 Pro", category = "Smartphones", description = "30W Schnellladen", price = BigDecimal.valueOf(10.00)))
                add(CompletedSaleData(title = "Bettlaken", category = "Textilien", description = "ohne Flecken", price = BigDecimal.valueOf(4.00)))
                add(CompletedSaleData(title = "Fallout Maske", category = "Freizeit", description = "leicht blutbeschmiert", price = BigDecimal.valueOf(11.00)))
                add(CompletedSaleData(title = "Boxershorts", category = "Textilien", description = "Müsste man noch waschen & kaum getragen", price = BigDecimal.valueOf(13.00)))
                add(CompletedSaleData(title = "Nachtschränkchen", category = "Möbelierung", description = "kastanienholz ohne Lampe", price = BigDecimal.valueOf(14.00)))
                add(CompletedSaleData(title = "Ibuprofen 400mg 20er Packung", category = "Medizin", description = "Aus Türkei zuverlässig mitgenommen", price = BigDecimal.valueOf(12.00)))
                add(CompletedSaleData(title = "Panini Sammelheft", category = "Spielzeug", description = "fast vollständig 1/108", price = BigDecimal.valueOf(15.00)))
                add(CompletedSaleData(title = "Briefmarkensammelheft", category = "Freizeit", description = "von meinem Urgroßvater", price = BigDecimal.valueOf(17.00)))
                add(CompletedSaleData(title = "Metin2- VMS +9", category = "Videospiel", description = "Ingame Schwert", price = BigDecimal.valueOf(18.48)))
            }
        } else {
            data
        }

    override fun onResetFilters(filters: MutableList<Filter>) { }

    override fun call(filterPagingOptions: FilterPagingOptions): Call<CompletedSaleDataPage> =
        app.api.saleCompletedGetByCreator(app.userId!!, filterPagingOptions)

    override fun onResult(success: Boolean) {
        if (JumbleApplication.isDebugLocal && success && (data.value?.size ?: 30) < 30) {
            data.value?.apply {
                add(CompletedSaleData(title = "2 Wochen Fitnessmitgliedschaft", category = "Freizeit", description = "mit Personaltrainer", price = BigDecimal.valueOf(69.49)))
                add(CompletedSaleData(title = "Deoroller", category = "Gesundheit", description = "vom DM (Axe)", price = BigDecimal.valueOf(02.19)))
                add(CompletedSaleData(title = "Couch braun", category = "Möbelierung", description = "wie neu", price = BigDecimal.valueOf(250.99)))
                add(CompletedSaleData(title = "Jack Daniels", category = "Lebensmittel", description = "schmeckt sehr gut", price = BigDecimal.valueOf(13.49)))
                add(CompletedSaleData(title = "Energydrinks", category = "Lebensmittel", description = "Redbull", price = BigDecimal.valueOf(10.00)))
                add(CompletedSaleData(title = "Simson S51 Zündkerze", category = "Fahrzeug", description = "Neu Originalverpackung", price = BigDecimal.valueOf(5.99)))
                add(CompletedSaleData(title = "Gitarre", category = "Freizeit", description = "neue Saiten", price = BigDecimal.valueOf(50.00)))
                add(CompletedSaleData(title = "Lauretaner Wasser", category = "Lebensmittel", description = "ungeöffnet", price = BigDecimal.valueOf(10.00)))
                add(CompletedSaleData(title = "VanMoof S3 Fahrrad", category = "Outdoor", description = "ohne Reifen", price = BigDecimal.valueOf(1499.49)))
                add(CompletedSaleData(title = "Taschenrechner CAS", category = "Elektronik", description = "mit Ladegerät", price = BigDecimal.valueOf(65.00)))
            }
            data.value = data.value // Notify observers.
        }
    }

    companion object {
        @JvmStatic private val TAG = OwnCompletedSaleRepository::class.simpleName
    }

}
