package com.jumble_x_sale.app.data.model

import com.jumble_x_sale.app.data.model.UserMessageData
import com.jumble_x_sale.app.data.model.util.PagingOptions
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class UserMessageDataPage(
    override var options: PagingOptions,
    override var data: List<UserMessageData>
) : DataPage<UserMessageData>
