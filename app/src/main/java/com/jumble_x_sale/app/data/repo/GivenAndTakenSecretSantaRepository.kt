package com.jumble_x_sale.app.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.CompletedSecretSantaData
import com.jumble_x_sale.app.data.model.CompletedSecretSantaDataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal

/**
 * Repository for completed secret santas for this member.
 */
class GivenAndTakenSecretSantaRepository(
    app: JumbleApplication
) : RepositoryBase<CompletedSecretSantaData, CompletedSecretSantaDataPage>(app) {

    override fun onRefresh(data: MutableList<CompletedSecretSantaData>) =
        if (JumbleApplication.isDebugLocal) {
            data.apply {
                add(CompletedSecretSantaData(description1 = "neu", description2 = "wie neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "gebrauchsspuren", description2 = "leichte gebrauchsspuren", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "wie neu", description2 = "gebraucht (Kuvert fehlt)", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "gebrauchsspuren", description2 = "wie neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "leichte gebrauchsspuren", description2 = "ungenutzt", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "wie neu", description2 = "neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "gebrauchsspuren", description2 = "neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "ungenutzt", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "leichte gebrauchsspuren", description2 = "wie neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "ungenutzt", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "gebrauchsspuren", description2 = "wie neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "leichte gebrauchsspuren", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "wie neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "leichte gebrauchsspuren", description2 = "ungenutzt", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "leichte gebrauchsspuren", description2 = "wie neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "leichte gebrauchsspuren", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
           }
        } else {
            data
        }

    override fun onResetFilters(filters: MutableList<Filter>) { }

    override fun call(filterPagingOptions: FilterPagingOptions): Call<CompletedSecretSantaDataPage> =
        app.api.secretSantaCompletedGetByMember(app.userId!!, filterPagingOptions)

    override fun onResult(success: Boolean) {
        if (JumbleApplication.isDebugLocal && success && (data.value?.size ?: 30) < 30) {
            data.value?.apply {
                add(CompletedSecretSantaData(description1 = "gebrauchsspuren", description2 = "wie neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "leichte gebrauchsspuren", description2 = "ungenutzt", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "wie neu", description2 = "neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "gebrauchsspuren", description2 = "neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "ungenutzt", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "leichte gebrauchsspuren", description2 = "wie neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "ungenutzt", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "gebrauchsspuren", description2 = "wie neu", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
                add(CompletedSecretSantaData(description1 = "neu", description2 = "leichte gebrauchsspuren", title1 = "Buch", category1 = "Buch", title2 = "Buch", category2 = "Buch"))
            }
            data.value = data.value // Notify observers.
        }
    }

    companion object {
        @JvmStatic private val TAG = GivenAndTakenSecretSantaRepository::class.simpleName
    }

}
