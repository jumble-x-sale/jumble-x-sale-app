@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.model.util

import com.jumble_x_sale.app.util.parseBigDecimal
import com.jumble_x_sale.app.util.parseInstant
import com.jumble_x_sale.app.util.parseLocalDate
import com.jumble_x_sale.app.util.parseLocalDateTime
import com.jumble_x_sale.app.util.parseLocalTime
import com.jumble_x_sale.app.util.toUuid
import com.jumble_x_sale.app.util.Utils
import com.squareup.moshi.JsonClass
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.Locale
import java.util.HashMap
import java.util.UUID

@JsonClass(generateAdapter = true)
class SingleValueFilter internal constructor(
    propertyName: String?,
    propertyType: DataType?,
    val operator: Operator?,
    val value: String?
) : Filter(propertyName, propertyType) {

    /**
     * An operator that takes one operand.
     */
    enum class Operator(val code: String) {
        /**
         * The givable value must be equaled.
         */
        EQUAL_TO(EQUAL_TO_CODE),
        /**
         * The givable value must not be equaled.
         */
        NOT_EQUAL_TO(NOT_EQUAL_TO_CODE),
        /**
         * The givable value must not be matched.
         */
        LESS_THAN_OR_EQUAL_TO(LESS_THAN_OR_EQUAL_TO_CODE),
        /**
         * The givable value must not be matched.
         */
        GREATER_THAN_OR_EQUAL_TO(GREATER_THAN_OR_EQUAL_TO_CODE),
        /**
         * The givable value must not be matched.
         */
        LESS_THAN(LESS_THAN_CODE),
        /**
         * The givable value must not be matched.
         */
        GREATER_THAN(GREATER_THAN_CODE);

        companion object {
            @JvmStatic private val valuesByCode = HashMap<String, Operator>()

            init {
                val types: Array<Operator> = values()
                for (i in types.indices) {
                    valuesByCode[types[i].code] = types[i]
                }
            }

            fun tryValueOf(idOrCode: String, defaultValue: Operator?): Operator? {
                val idOrCodeUpper = idOrCode.toUpperCase(Locale.ROOT)
                var value = valuesByCode[idOrCodeUpper]
                if (value == null) {
                    value = Utils.tryValueOf(idOrCodeUpper, defaultValue)
                }
                return value
            }

            fun tryValueOf(idOrCode: String): Operator? = tryValueOf(idOrCode, null)
        }
    }

    constructor() : this(null, null, null, null)

    fun valueAsUUID(): UUID? = this.value.toUuid()
    fun valueAsDecimal(): BigDecimal? = this.value.parseBigDecimal()
    fun valueAsInstant(): Instant? = this.value.parseInstant()
    fun valueAsDate(): LocalDate? = this.value.parseLocalDate()
    fun valueAsDateTime(): LocalDateTime? = this.value.parseLocalDateTime()
    fun valueAsTime(): LocalTime? = this.value.parseLocalTime()

    companion object {
        private const val EQUAL_TO_CODE = "EQ"
        private const val NOT_EQUAL_TO_CODE = "NEQ"
        private const val LESS_THAN_OR_EQUAL_TO_CODE = "LTEQ"
        private const val GREATER_THAN_OR_EQUAL_TO_CODE = "GTEQ"
        private const val LESS_THAN_CODE = "LT"
        private const val GREATER_THAN_CODE = "GT"
        private const val PN_VALUE = "value"
    }
}
