package com.jumble_x_sale.app.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.CompletedSaleMessageData
import com.jumble_x_sale.app.data.model.CompletedSaleMessageDataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import com.jumble_x_sale.app.util.TimeUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Repository for messages related to completed sales from this member.
 */
class CompletedSaleMessageRepository(
    app: JumbleApplication
) : RepositoryBase<CompletedSaleMessageData, CompletedSaleMessageDataPage>(app) {

    var completedSale: CompletedSaleData? = null
        set(value) {
            field = value
            refresh()
        }

    override fun onRefresh(data: MutableList<CompletedSaleMessageData>) =
        if (JumbleApplication.isDebugLocal) {
            data.apply {
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 1), message = "Xbox 360 verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 2), message = "Tamagotchi verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 3), message = "Hisense 55 Zoll Fernsehr verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 4), message = "Focusrite Interface verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 5), message = "Acer 27 Zoll Monitor verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 6), message = "Harry Potter Band 7 verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 7), message = "Spongebob Puppe verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 8), message = "Toilettenpapier unbenutzt verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 9), message = "Colin verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 10), message = "Kant (Buch) verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 11), message = "Couch (braun) verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 12), message = "BGB 2012 verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 13), message = "Rasierapparat (unbenutzt) verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 14), message = "Samsung S20 verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 15), message = "Wasserbett verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 16), message = "Powerbank 20.000mah verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 17), message = "Shisha Adlerformat verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 18), message = "50 LEDs verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 19), message = "Fingerhut verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 20), message = "PS3 Controller verkauft!"))
           }
        } else {
            data
        }

    override fun onResetFilters(filters: MutableList<Filter>) { }

    override fun call(filterPagingOptions: FilterPagingOptions): Call<CompletedSaleMessageDataPage> =
        app.api.saleCompletedMessageGetBySaleCreator(app.userId!!, filterPagingOptions)

    override fun onResult(success: Boolean) {
        if (JumbleApplication.isDebugLocal && success && (data.value?.size ?: 30) < 30) {
            data.value?.apply {
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 21), message = "Battlefield 2 verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 22), message = "Amazon Kindle verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 23), message = "Gitarre verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 24), message = "LS19 - Grundversion verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 25), message = "Vodka Gorbatschow (special Edition) verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 26), message = "FCB Schal verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 27), message = "Amazon Gutschein 20€ verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 28), message = "Gemälde von Herrn Avemarg verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 29), message = "Socken schwarz verkauft!"))
                add(CompletedSaleMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 30), message = "Geschichtsbuch 1900-2000 verkauft!"))
            }
            data.value = data.value
        }
    }

    companion object {
        @JvmStatic private val TAG = CompletedSaleMessageRepository::class.simpleName
    }

}
