@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.network

import com.jumble_x_sale.app.BuildConfig
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.CompletedSecretSantaData
import com.jumble_x_sale.app.data.model.CompletedSecretSantaDataPage
import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.CompletedSaleDataPage
import com.jumble_x_sale.app.data.model.CompletedSaleMessageData
import com.jumble_x_sale.app.data.model.CompletedSaleMessageDataPage
import com.jumble_x_sale.app.data.model.Credentials
import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.data.model.SaleOfferDataPage
import com.jumble_x_sale.app.data.model.SaleOfferMessageData
import com.jumble_x_sale.app.data.model.SaleOfferMessageDataPage
import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.data.model.SecretSantaOfferDataPage
import com.jumble_x_sale.app.data.model.Session
import com.jumble_x_sale.app.data.model.UserData
import com.jumble_x_sale.app.data.model.UserDataPage
import com.jumble_x_sale.app.data.model.UserMessageData
import com.jumble_x_sale.app.data.model.UserMessageDataPage
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import java.util.UUID
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Query

interface JumbleApi {

    // -------------------------------------------------------------------------

    /** Create a user. */
    @Headers("Content-Type: application/json", "Accept: text/plain")
    @POST("user")
    fun userCreate(@Body data: UserData): Call<UUID>

    /** Update a user. */
    @Headers("Content-Type: application/json")
    @PUT("user")
    fun userUpdate(@Body data: UserData): Call<Unit>

    /** Delete a user. */
    @PUT("user")
    fun userDelete(@Path("userId") userId: UUID): Call<Unit>

    /** Get users matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("user/get")
    fun userGet(@Body filterPagingOptions: FilterPagingOptions): Call<UserData>

    /** Get a user by his email. */
    @Headers("Accept: application/json")
    @POST("user/byEmail/{email}")
    fun userByEmail(@Path("email") id: String): Call<UserData>

    /** Login with credentials. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("user/login")
    fun userLogin(@Body credentials: Credentials): Call<Session>

    // -------------------------------------------------------------------------

    /** Create a user message. */
    @Headers("Content-Type: application/json", "Accept: text/plain")
    @POST("user/message")
    fun userMessageCreate(@Body data: UserMessageData): Call<UUID>

    /** Update a user message. */
    @Headers("Content-Type: application/json")
    @PUT("user/message")
    fun userMessageUpdate(@Body data: UserMessageData): Call<Unit>

    /** Delete a user message. */
    @DELETE("user/message/{userMessageId}")
    fun userMessageDelete(@Path("userMessageId") userMessageId: UUID): Call<Unit>

    /** Get user messages matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("user/message/get")
    fun userMessageGet(@Body filterPagingOptions: FilterPagingOptions): Call<UserMessageData>

    /** Get user messages matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("user/message/getByMember/{memberId}")
    fun userMessageGetByMember(
        @Path("memberId") memberId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<UserMessageDataPage>

    /** Get user messages matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("user/message/getByReceiver/{receiverId}")
    fun userMessageGetByReceiver(
        @Path("receiverId") receiverId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<UserMessageData>

    // -------------------------------------------------------------------------

    /** Create a secret santa offer. */
    @Headers("Content-Type: application/json", "Accept: text/plain")
    @POST("secretSanta/offer")
    fun secretSantaOfferCreate(@Body data: SecretSantaOfferData): Call<UUID>

    /** Update a secret santa offer. */
    @Headers("Content-Type: application/json")
    @PUT("secretSanta/offer")
    fun secretSantaOfferUpdate(@Body data: SecretSantaOfferData): Call<Unit>

    /** Delete a secret santa offer. */
    @DELETE("secretSanta/offer/{secretSantaOfferId}")
    fun secretSantaOfferDelete(@Path("secretSantaOfferId") secretSantaOfferId: UUID): Call<Unit>

    /** Get secret santa offers matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("secretSanta/offer/get")
    fun secretSantaOfferGet(@Body filterPagingOptions: FilterPagingOptions): Call<SecretSantaOfferDataPage>

    /** Get secret santa offers matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("secretSanta/offer/getByCreator/{creatorId}")
    fun secretSantaOfferGetByCreator(
        @Path("creatorId") creatorId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<SecretSantaOfferDataPage>

    /** Get a match for a secret santa offer. */
    @POST("secretSanta/offer/match/{secretSantaOfferId}")
    fun secretSantaOfferMatch(@Path("secretSantaOfferId") secretSantaOfferId: UUID): Call<UUID>

    // -------------------------------------------------------------------------

    /** Create a sale offer. */
    @Headers("Content-Type: application/json", "Accept: text/plain")
    @POST("sale/offer")
    fun saleOfferCreate(@Body data: SaleOfferData): Call<UUID>

    /** Update a sale offer. */
    @Headers("Content-Type: application/json")
    @PUT("sale/offer")
    fun saleOfferUpdate(@Body data: SaleOfferData): Call<Unit>

    /** Delete a sale offer. */
    @DELETE("sale/offer/{saleOfferId}")
    fun saleOfferDelete(@Path("saleOfferId") saleOfferId: UUID): Call<Unit>

    /** Get sale offers matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("sale/offer/get")
    fun saleOfferGet(@Body filterPagingOptions: FilterPagingOptions): Call<SaleOfferDataPage>

    /** Get sale offers matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("sale/offer/getByCreator/{creatorId}")
    fun saleOfferGetByCreator(
        @Path("creatorId") creatorId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<SaleOfferDataPage>

    /** Get sale offers matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("sale/offer/getExceptCreator/{creatorId}")
    fun saleOfferGetExceptCreator(
        @Path("creatorId") creatorId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<SaleOfferDataPage>

    /** Accept an open sale offer. */
    @POST("sale/offer/accept/{saleOfferId}")
    fun saleOfferAccept(@Path("saleOfferId") saleOfferId: UUID): Call<UUID>

    // -------------------------------------------------------------------------

    /** Create a sale offer message. */
    @Headers("Content-Type: application/json", "Accept: text/plain")
    @POST("sale/offer/message")
    fun saleOfferMessageCreate(@Body data: SaleOfferMessageData): Call<UUID>

    /** Update a sale offer message. */
    @Headers("Content-Type: application/json")
    @PUT("sale/offer/message")
    fun saleOfferMessageUpdate(@Body data: SaleOfferMessageData): Call<Unit>

    /** Delete a sale offer message. */
    @DELETE("sale/offer/message/{saleOfferMessageId}")
    fun saleOfferMessageDelete(@Path("saleOfferMessageId") saleOfferMessageId: UUID): Call<Unit>

    /** Get sale offer messages matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("sale/offer/message/get")
    fun saleOfferMessageGet(@Body filterPagingOptions: FilterPagingOptions): Call<SaleOfferMessageData>

    /** Get sale offer messages for a user that match the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("sale/offer/message/getBySaleCreator/{saleCreatorId}")
    fun saleOfferMessageGetBySaleCreator(
        @Path("saleCreatorId") saleCreatorId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<SaleOfferMessageDataPage>

    /** Get sale offer messages for a sale offer that match the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("sale/offer/message/getBySale/{saleOfferId}")
    fun saleOfferMessageGetBySale(
        @Path("saleOfferId") saleOfferId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<SaleOfferMessageDataPage>

    // -------------------------------------------------------------------------

    /** Get completed secret santas matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("secretSanta/completed/get")
    fun secretSantaCompletedGet(@Body filterPagingOptions: FilterPagingOptions): Call<CompletedSecretSantaData>

    /** Get a completed secret santa by involved member. */
    @Headers("Accept: application/json")
    @POST("secretSanta/completed/getByMember/{memberId}")
    fun secretSantaCompletedGetByMember(
        @Path("memberId") memberId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<CompletedSecretSantaDataPage>

    // -------------------------------------------------------------------------

    /** Get completed sales matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("sale/completed/get")
    fun saleCompletedGet(@Body filterPagingOptions: FilterPagingOptions): Call<CompletedSaleData>

    /** Get a completed sale by its creator. */
    @Headers("Accept: application/json")
    @POST("sale/completed/getByCreator/{creatorId}")
    fun saleCompletedGetByCreator(
        @Path("creatorId") creatorId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<CompletedSaleDataPage>

    /** Get a completed sale by its creator. */
    @Headers("Accept: application/json")
    @POST("sale/completed/getExceptCreator/{creatorId}")
    fun saleCompletedGetExceptCreator(
        @Path("creatorId") creatorId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<CompletedSaleDataPage>

    /** Get a completed sale by its buyer. */
    @Headers("Accept: application/json")
    @GET("sale/completed/byBuyer/{buyerId}")
    fun saleCompletedGetByBuyer(
        @Path("buyerId") buyerId: UUID,
        @Query("pageNumber") pageNumber: Int,
        @Query("pageSize") pageSize: Int
    ): Call<CompletedSaleData>

    // -------------------------------------------------------------------------

    /** Get completed sales matching the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("sale/completed/message/get")
    fun saleCompletedMessageGet(@Body filterPagingOptions: FilterPagingOptions): Call<CompletedSaleMessageData>

    /** Get completed sale messages for a user that match the filter paging options. */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("sale/completed/message/getBySaleCreator/{saleCreatorId}")
    fun saleCompletedMessageGetBySaleCreator(
        @Path("saleCreatorId") saleCreatorId: UUID,
        @Body filterPagingOptions: FilterPagingOptions
    ): Call<CompletedSaleMessageDataPage>

    /** Get completed sale messages matching the filter paging options. */
    @Headers("Accept: application/json")
    @GET("sale/completed/message/bySale/{completedSaleId}")
    fun saleCompletedMessageGetBySale(
        @Path("completedSaleId") completedSaleId: UUID,
        @Query("pageNumber") pageNumber: Int,
        @Query("pageSize") pageSize: Int
    ): Call<CompletedSaleMessageData>

    // -------------------------------------------------------------------------

    companion object {

        @Suppress("ConstantConditionIf")
        private val API_BASE_URL =
            if (JumbleApplication.isDebugLocal) {
                "http://192.168.178.38:8080/api/v1/"
            } else {
                "https://jumblexsale.azurewebsites.net/api/v1/"
            }

        private fun provideOkHttpClient(app: JumbleApplication) = OkHttpClient.Builder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .addInterceptor(AuthorizationInterceptor(app))
                .authenticator(TokenRefreshAuthenticator(app))
                .build()

        private fun getRetrofit(app: JumbleApplication) = Retrofit.Builder()
                .client(provideOkHttpClient(app))
                .addConverterFactory(MoshiConverterFactory.create(app.moshi).asLenient())
                .baseUrl(JumbleApi.API_BASE_URL)
                .build()

        fun create(app: JumbleApplication) = getRetrofit(app).create(JumbleApi::class.java) as JumbleApi

    }
}
