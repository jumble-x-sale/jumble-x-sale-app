package com.jumble_x_sale.app.data.model

import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.data.model.util.PagingOptions
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class SaleOfferDataPage(
    override var options: PagingOptions,
    override var data: List<SaleOfferData>
) : DataPage<SaleOfferData>
