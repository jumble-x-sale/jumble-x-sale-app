package com.jumble_x_sale.app.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.data.model.SecretSantaOfferDataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal

/**
 * Repository for secret santa offers from this member.
 */
class SecretSantaOfferRepository(
    app: JumbleApplication
) : RepositoryBase<SecretSantaOfferData, SecretSantaOfferDataPage>(app) {

    override fun onRefresh(data: MutableList<SecretSantaOfferData>) =
        if (JumbleApplication.isDebugLocal) {
            data.apply {
                add(SecretSantaOfferData(description = "guter Zustand", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "leichte Gebrauchsspuren", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "guter Zustand", title = "Buch"))
                add(SecretSantaOfferData(description = "wie neu", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "abgenutzt", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "abgenutzt", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "leichte Gebrauchsspuren", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "abgenutzt", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "guter Zustand", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "wie neu", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "leichte Gebrauchsspuren", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "abgenutzt", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "wie neu", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "leichte Gebrauchsspuren", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "abgenutzt", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "guter Zustand", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "guter Zustand", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "guter Zustand", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "abgenutzt", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "leichte Gebrauchsspuren", title = "Buch", category = "Buch"))
            }
        } else {
            data
        }

    override fun onResetFilters(filters: MutableList<Filter>) { }

    override fun call(filterPagingOptions: FilterPagingOptions): Call<SecretSantaOfferDataPage> =
        app.api.secretSantaOfferGetByCreator(app.userId!!, filterPagingOptions)

    override fun onResult(success: Boolean) {
        if (JumbleApplication.isDebugLocal && success && (data.value?.size ?: 30) < 30) {
            data.value?.apply {
                add(SecretSantaOfferData(description = "wie neu", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "abgenutzt", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "guter Zustand", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "leichte Gebrauchsspuren", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "abgenutzt", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "guter Zustand", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "wie neu", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "leichte Gebrauchsspuren", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "guter Zustand", title = "Buch", category = "Buch"))
                add(SecretSantaOfferData(description = "abgenutzt", title = "Buch", category = "Buch"))
            }
            data.value = data.value // Notify observers.
        }
    }

    companion object {
        @JvmStatic private val TAG = SecretSantaOfferRepository::class.simpleName
    }

}
