@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.model.util

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class FilterPagingOptions(
    pageNumber: Long,
    pageSize: Long = 20,
    rowCount: Long = 0,
    var filters: List<Filter> = listOf<Filter>()
) : PagingOptions(pageNumber, pageSize, rowCount) {

    constructor() : this(0L, 0L, 0L, listOf<Filter>())

}
