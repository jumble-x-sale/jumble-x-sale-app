package com.jumble_x_sale.app.data.model

import com.jumble_x_sale.app.data.model.SaleOfferMessageData
import com.jumble_x_sale.app.data.model.util.PagingOptions
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class SaleOfferMessageDataPage(
    override var options: PagingOptions,
    override var data: List<SaleOfferMessageData>
) : DataPage<SaleOfferMessageData>
