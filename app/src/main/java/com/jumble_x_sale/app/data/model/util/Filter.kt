@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.model.util

import com.jumble_x_sale.app.util.Utils
import java.math.BigDecimal
import java.util.Locale
import java.util.UUID
import kotlin.collections.HashMap

abstract class Filter protected constructor(
    var propertyName: String?,
    var propertyType: DataType?
) {

    /**
     * The type of filter value.
     */
    enum class DataType constructor(val code: String) {
        /**
         * A [UUID].
         */
        UUID("ID"),
        /**
         * A [BigDecimal].
         */
        NUMBER("N"),
        /**
         * An [Instant].
         */
        INSTANT("I"),
        /**
         * A [LocalDate].
         */
        DATE("D"),
        /**
         * A [LocalDateTime].
         */
        DATE_TIME("DT"),
        /**
         * A [LocalTime].
         */
        TIME("T"),
        /**
         * A [String].
         */
        STRING("S");

        companion object {
            @JvmStatic private val valuesByCode = HashMap<String, DataType>()

            init {
                val types: Array<DataType> = values()
                for (i in types.indices) {
                    valuesByCode[types[i].code] = types[i]
                }
            }

            fun tryValueOf(idOrCode: String, defaultValue: DataType?): DataType? {
                val idOrCodeUpper = idOrCode.toUpperCase(Locale.ROOT)
                var value: DataType? = valuesByCode[idOrCodeUpper]
                if (value == null) {
                    value = Utils.tryValueOf(idOrCodeUpper, defaultValue)
                }
                return value
            }

            fun tryValueOf(idOrCode: String): DataType? = tryValueOf(idOrCode, null)
        }
    }

    companion object {
        @JvmStatic val EMPTY_ARRAY: Array<Filter> = arrayOf()
        @JvmStatic val EMPTY_LIST: List<Filter> = listOf()

        protected const val PN_PROPERTY_NAME: String = "propertyName"
        protected const val PN_PROPERTY_TYPE: String = "propertyType"
        protected const val PN_OPERATOR: String = "operator"

        fun equal(propertyName: String, value: String) =
            SingleValueFilter(propertyName, DataType.STRING, SingleValueFilter.Operator.EQUAL_TO, value)

        fun equal(propertyName: String, value: UUID) =
            SingleValueFilter(propertyName, DataType.UUID, SingleValueFilter.Operator.EQUAL_TO, value.toString())

        fun notEqual(propertyName: String, value: String) =
            SingleValueFilter(propertyName, DataType.STRING, SingleValueFilter.Operator.NOT_EQUAL_TO, value)

        fun notEqual(propertyName: String, value: UUID) =
            SingleValueFilter(propertyName, DataType.UUID, SingleValueFilter.Operator.NOT_EQUAL_TO, value.toString())

        fun lesserEqual(propertyName: String, propertyType: DataType, value: String) =
            SingleValueFilter(propertyName, propertyType, SingleValueFilter.Operator.LESS_THAN_OR_EQUAL_TO, value)

        fun greaterEqual(propertyName: String, propertyType: DataType, value: String) =
            SingleValueFilter(propertyName, propertyType, SingleValueFilter.Operator.GREATER_THAN_OR_EQUAL_TO, value)

        fun lesser(propertyName: String, propertyType: DataType, value: String) =
            SingleValueFilter(propertyName, propertyType, SingleValueFilter.Operator.LESS_THAN, value)

        fun greater(propertyName: String, propertyType: DataType, value: String) =
            SingleValueFilter(propertyName, propertyType, SingleValueFilter.Operator.GREATER_THAN, value)

        fun between(propertyName: String, propertyType: DataType, minValue: String, maxValue: String) =
            DualValueFilter(propertyName, propertyType, DualValueFilter.Operator.BETWEEN, minValue, maxValue)

        fun inBetween(propertyName: String, propertyType: DataType, minValue: String, maxValue: String) =
            DualValueFilter(propertyName, propertyType, DualValueFilter.Operator.IN_BETWEEN, minValue, maxValue)

        fun inArray(propertyName: String, propertyType: DataType, vararg values: String) =
            MultiValueFilter(propertyName, propertyType, MultiValueFilter.Operator.IN, arrayOf(*values))

        fun notInArray(propertyName: String, propertyType: DataType, vararg values: String) =
            MultiValueFilter(propertyName, propertyType, MultiValueFilter.Operator.NOT_IN, arrayOf(*values))
    }
}
