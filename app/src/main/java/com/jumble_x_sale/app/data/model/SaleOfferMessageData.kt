package com.jumble_x_sale.app.data.model

import com.squareup.moshi.JsonClass
import java.time.Instant
import java.util.UUID

@JsonClass(generateAdapter = true)
data class SaleOfferMessageData(
    var id: UUID? = null,
    var senderId: UUID? = null,
    var creationInstant: Instant? = null,
    var message: String? = null,
    var saleOfferId: UUID? = null
) {

    companion object {
        const val ID = "id"
        const val SENDER_ID = "senderId"
        const val CREATION_INSTANT = "creationInstant"
        const val MESSAGE = "message"
        const val SALE_OFFER_ID = "saleOfferId"
    }

}
