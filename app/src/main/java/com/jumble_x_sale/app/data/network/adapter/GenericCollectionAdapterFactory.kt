@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.network.adapter

import android.annotation.SuppressLint
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * [https://pulseadvancedmedia.co.uk/java/moshi-adapters-for-platform-types-like-arraylist-and-linkedlist/]
 * Altered to match API level 23.
 */
class GenericCollectionAdapterFactory<TCollection : MutableCollection<*>>(
    private val collectionClazz: Class<TCollection>,
    private val createEmptyCollection: () -> MutableCollection<Any>
) : JsonAdapter.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun create(type: Type, annotations: MutableSet<out Annotation>, moshi: Moshi): JsonAdapter<*>? {
        val paramType = type as? ParameterizedType ?: return null
        if (paramType.rawType != collectionClazz) return null
        if (paramType.actualTypeArguments.size != 1) return null
        val argType = paramType.actualTypeArguments[0] as Class<Any>

        return GenericCollectionAdapter(argType, moshi, createEmptyCollection)
    }
}
