package com.jumble_x_sale.app.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import java.time.Instant
import java.util.UUID

@JsonClass(generateAdapter = true)
@Entity(tableName = "secretSantaOffers")
open class SecretSantaOfferData(
    @PrimaryKey
    var id: UUID? = null,
    var creatorId: UUID? = null,
    var creationInstant: Instant? = null,
    var category: String? = null,
    var title: String? = null,
    var description: String? = null,
    var imageUrls: Array<String>? = null,
    var mainImageData: String? = null
) {

    companion object {
        const val ID = "id"
        const val CREATOR_ID = "creatorId"
        const val CREATION_INSTANT = "creationInstant"
        const val CATEGORY = "category"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
    }

}
