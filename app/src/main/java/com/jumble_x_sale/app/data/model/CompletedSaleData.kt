package com.jumble_x_sale.app.data.model

import com.squareup.moshi.JsonClass
import java.time.Instant
import java.math.BigDecimal
import java.util.UUID

/**
 * CompletedSale contains information about a SaleOffer and its buyer.
 */
@JsonClass(generateAdapter = true)
data class CompletedSaleData(
    var id: UUID? = null,
    var creatorId: UUID? = null,
    var creationInstant: Instant? = null,
    var category: String? = null,
    var title: String? = null,
    var description: String? = null,
    var imageUrls: Array<String>? = null,
    var price: BigDecimal? = null,
    var buyerId: UUID? = null,
    var completionInstant: Instant? = null
) {

    companion object {
        const val ID = "id"
        const val CREATOR_ID = "creatorId"
        const val CREATION_INSTANT = "creationInstant"
        const val CATEGORY = "category"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val PRICE = "price"
        const val BUYER_ID = "buyerId"
        const val COMPLETION_INSTANT = "completionInstant"
    }

}
