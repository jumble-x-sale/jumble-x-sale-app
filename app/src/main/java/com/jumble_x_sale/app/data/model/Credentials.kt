package com.jumble_x_sale.app.data.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Credentials(
    var email: String,
    var password: String)
