package com.jumble_x_sale.app.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.DataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import com.jumble_x_sale.app.data.model.util.PagingOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Repository base.
 */
abstract class RepositoryBase<TData, TPage>(
    protected val app: JumbleApplication
)
    where TPage : DataPage<TData>
{

    var data = MutableLiveData<MutableList<TData>>().apply {
            value = onRefresh(mutableListOf<TData>())
        }
        protected set
    protected var lastResultPagingOptions = PagingOptions()

    var filters: List<Filter> = mutableListOf<Filter>().apply { onResetFilters(this) }
        set(value) {
            field = mutableListOf<Filter>().apply {
                onResetFilters(this)
                addAll(value)
                refresh()
            }
        }

    /** True if we are still waiting for the last set of data to load. */
    protected var loading: Boolean = false

    /** Executed when refreshing data. May be used to inject test data aside from network calls. */
    protected open fun onRefresh(data: MutableList<TData>) = data

    /** Executed when resetting the filters. May be used to inject filters. */
    protected abstract fun onResetFilters(filters: MutableList<Filter>)

    /** The call to load data. */
    protected abstract fun call(filterPagingOptions: FilterPagingOptions): Call<TPage>

    /** Executed when the result of a call is available. May be used to inject test data aside from network calls. */
    protected abstract fun onResult(success: Boolean)

    /** Refresh data. */
    fun refresh() {
        this.data.value = onRefresh(mutableListOf<TData>())
        lastResultPagingOptions = PagingOptions()
        loadMore()
    }

    /** Load more data. */
    fun loadMore() {
        if (!loading && app.userId != null && (
            lastResultPagingOptions.pageCount == 0L ||
            lastResultPagingOptions.pageCount > lastResultPagingOptions.pageNumber)
        ) {
            loading = true
            call(lastResultPagingOptions.nextPage(filters))
                .enqueue(object : Callback<TPage> {
                override fun onResponse(call: Call<TPage>?, response: Response<TPage>?) {
                    if (response != null && response.isSuccessful && response.body() != null) {
                        Log.i(TAG, "OK")
                        val page = response.body()!!
                        lastResultPagingOptions = page.options
                        data.value?.addAll(page.data)
                        onResult(true)
                    } else if (response != null && response.code() == 401) {
                        Log.w(TAG, "Unauthorized")
                        app.logout()
                    } else {
                        Log.w(TAG, response?.toString() ?: "")
                        onResult(false)
                    }
                    loading = false
                }

                override fun onFailure(call: Call<TPage?>?, t: Throwable?) {
                    Log.w(TAG, t.toString())
                    onResult(false)
                    loading = false
                }
            })
        }
    }

    companion object {
        @JvmStatic private val TAG = RepositoryBase::class.simpleName
    }

}
