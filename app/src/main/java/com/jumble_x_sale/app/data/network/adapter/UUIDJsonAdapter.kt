@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.network.adapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.UUID

class UUIDJsonAdapter {

    @FromJson
    fun fromJson(json: String): UUID {
        return UUID.fromString(json)!!
    }

    @ToJson
    fun toJson(value: UUID): String {
        return value.toString()
    }

}
