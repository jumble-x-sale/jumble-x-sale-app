package com.jumble_x_sale.app.data.model

import com.squareup.moshi.JsonClass
import java.time.Instant
import java.util.UUID

/**
 * CompletedSale contains information about a SaleOffer and its buyer.
 */
@JsonClass(generateAdapter = true)
data class CompletedSecretSantaData(
    var id: UUID? = null,
    var completionInstant: Instant? = null,
    var creatorId1: UUID? = null,
    var creationInstant1: Instant? = null,
    var category1: String? = null,
    var title1: String? = null,
    var description1: String? = null,
    var creatorId2: UUID? = null,
    var creationInstant2: Instant? = null,
    var category2: String? = null,
    var title2: String? = null,
    var description2: String? = null
) {

    companion object {
        const val ID = "id"
        const val CREATOR_ID1 = "creatorId1"
        const val CREATION_INSTANT1 = "creationInstant1"
        const val CATEGORY1 = "category1"
        const val TITLE1 = "title1"
        const val DESCRIPTION1 = "description1"
        const val CREATOR_ID2 = "creatorId2"
        const val CREATION_INSTANT2 = "creationInstant2"
        const val CATEGORY2 = "category2"
        const val TITLE2 = "title2"
        const val DESCRIPTION2 = "description2"
    }

}
