package com.jumble_x_sale.app.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.SaleOfferData
import com.jumble_x_sale.app.data.model.SaleOfferMessageData
import com.jumble_x_sale.app.data.model.SaleOfferMessageDataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import com.jumble_x_sale.app.util.TimeUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Repository for messages related to sale offers from this member.
 */
class SaleOfferMessageRepository(
    app: JumbleApplication
) : RepositoryBase<SaleOfferMessageData, SaleOfferMessageDataPage>(app) {

    var saleOffer: SaleOfferData? = null
        set(value) {
            field = value
            refresh()
        }

    override fun onRefresh(data: MutableList<SaleOfferMessageData>) =
        if (JumbleApplication.isDebugLocal) {
            data.apply {
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 1), message = "Verhandlungsbasis?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 2), message = "Festpreis?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 3), message = "Zustand?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 4), message = "Zubehör?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 5), message = "Originalpreis?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 6), message = "Kaufdatum?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 7), message = "Kaufbeleg?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 8), message = "Zustand?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 9), message = "Geschmack?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 11), message = "Originalverpackung?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 12), message = "Originalpreis?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 10), message = "Geschmack?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 13), message = "Zustand?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 14), message = "Festpreis?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 15), message = "Kaufbeleg?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 16), message = "Eigene Erfahrung?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 17), message = "Anzahl?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 18), message = "Verhandlungsbasis?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 19), message = "Zubehör?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 20), message = "Zustand"))
            }
        } else {
            data
        }

    override fun onResetFilters(filters: MutableList<Filter>) { }

    override fun call(filterPagingOptions: FilterPagingOptions): Call<SaleOfferMessageDataPage> =
        if (saleOffer?.id == null) {
            app.api.saleOfferMessageGetBySaleCreator(app.userId!!, filterPagingOptions)
        } else {
            app.api.saleOfferMessageGetBySale(saleOffer?.id!!, filterPagingOptions)
        }

    override fun onResult(success: Boolean) {
        if (JumbleApplication.isDebugLocal && success && (data.value?.size ?: 30) < 30) {
            data.value?.apply {
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 21), message = "Leistung?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 22), message = "Speicherplatz?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 23), message = "Version?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 24), message = "Verbrauch?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 25), message = "Nutzungsdauer?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 26), message = "Stromverbrauch?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 27), message = "Kaufdatum?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 28), message = "Originalverpackung?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 29), message = "Eigene Erfahrung?"))
                add(SaleOfferMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 30), message = "Benutzt?"))
            }
            data.value = data.value
        }
    }

    companion object {
        @JvmStatic private val TAG = SaleOfferMessageRepository::class.simpleName
    }

}
