@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.model.util

import com.jumble_x_sale.app.util.Utils
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class PagingOptions(
    var pageNumber: Long,
    var pageSize: Long,
    rowCount: Long
) : ResultOptions() {

    // override fun getStartPosition(): Long = (this.pageNumber - 1L) * this.pageSize
    // override fun getResultCount(): Long = this.pageSize

    override val startPosition: Long; get() = (this.pageNumber - 1L) * this.pageSize
    override val resultCount: Long; get() = this.pageSize

    var pageCount: Long = 0L
    var rowCount: Long = 0L
        get() = field
        set(value) {
            field = value
            if (this.pageCount == 0L) {
                this.pageCount = Utils.divideCeil(value, this.pageSize)
            }
        }

    init {
        if (pageNumber <= 0L || pageSize <= 0L) {
            // this.pageNumber = 1L // This server-side line is here explicitly unwanted!
            this.pageSize = DEFAULT_PAGE_SIZE
        }
        this.rowCount = rowCount
    }

    constructor() : this(0L, 0L, 0L)

    fun nextPage(filters: List<Filter>) = FilterPagingOptions(
            pageNumber = pageNumber + 1,
            pageSize = pageSize,
            filters = filters)

    companion object {
        const val DEFAULT_PAGE_SIZE: Long = 20L

        fun start() = PagingOptions(0L, 0L, 0L)
    }
}
