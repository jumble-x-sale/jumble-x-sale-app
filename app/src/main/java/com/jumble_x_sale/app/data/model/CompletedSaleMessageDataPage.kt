package com.jumble_x_sale.app.data.model

import com.jumble_x_sale.app.data.model.CompletedSaleMessageData
import com.jumble_x_sale.app.data.model.util.PagingOptions
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class CompletedSaleMessageDataPage(
    override var options: PagingOptions,
    override var data: List<CompletedSaleMessageData>
) : DataPage<CompletedSaleMessageData>
