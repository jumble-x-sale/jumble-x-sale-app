package com.jumble_x_sale.app.data.model

import androidx.room.Entity
import com.squareup.moshi.JsonClass
import java.math.BigDecimal
import java.time.Instant
import java.util.UUID

@JsonClass(generateAdapter = true)
@Entity(tableName = "saleOffers")
class SaleOfferData(
    id: UUID? = null,
    creatorId: UUID? = null,
    creationInstant: Instant? = null,
    category: String? = null,
    title: String? = null,
    description: String? = null,
    imageUrls: Array<String>? = null,
    mainImageData: String? = null,
    var price: BigDecimal? = null
) : SecretSantaOfferData(id, creatorId, creationInstant, category, title, description, imageUrls, mainImageData) {

    companion object {
        const val ID = "id"
        const val CREATOR_ID = "creatorId"
        const val CREATION_INSTANT = "creationInstant"
        const val CATEGORY = "category"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val PRICE = "price"
    }

}
