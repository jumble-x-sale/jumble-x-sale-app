package com.jumble_x_sale.app.data.model

import com.jumble_x_sale.app.data.model.util.PagingOptions
import com.jumble_x_sale.app.data.model.SaleOfferDataPage

interface DataPage<T> {
    var options: PagingOptions
    var data: List<T>
}
