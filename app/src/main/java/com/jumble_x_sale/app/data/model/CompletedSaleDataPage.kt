package com.jumble_x_sale.app.data.model

import com.jumble_x_sale.app.data.model.CompletedSaleData
import com.jumble_x_sale.app.data.model.util.PagingOptions
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class CompletedSaleDataPage(
    override var options: PagingOptions,
    override var data: List<CompletedSaleData>
) : DataPage<CompletedSaleData>
