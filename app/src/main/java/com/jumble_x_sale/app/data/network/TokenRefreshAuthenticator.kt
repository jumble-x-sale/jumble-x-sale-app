package com.jumble_x_sale.app.data.network

import android.util.Log
import com.jumble_x_sale.app.core.JumbleApplication
import java.io.IOException
import okhttp3.Authenticator
import okhttp3.Response
import okhttp3.Request
import okhttp3.Route

class TokenRefreshAuthenticator(private val app: JumbleApplication) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? = response.createRequestWithAuth()

    private fun Response.createRequestWithAuth(): Request? =
        try {
            val loginResponse = app.api.userLogin(app.credentials).execute()
            if (loginResponse.isSuccessful) {
                app.setSession(loginResponse.body())
                request().setAuth(app)
            } else {
                null
            }

        } catch (error: Throwable) {
            Log.e(TAG, error.toString())
            null
        }

    private fun Request.setAuth(app: JumbleApplication): Request = newBuilder()
            .header(AUTH, "Bearer ${app.accessToken}")
            .build()

    private val Response.retryCount: Int
        get() {
            var currentResponse = priorResponse()
            var result = 0
            while (currentResponse != null) {
                result++
                currentResponse = currentResponse.priorResponse()
            }
            return result
        }

    companion object {
        @JvmStatic private val TAG = TokenRefreshAuthenticator::class.java.simpleName
        private const val AUTH = "Authorization";
    }

}
