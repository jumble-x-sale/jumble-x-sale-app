package com.jumble_x_sale.app.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jumble_x_sale.app.core.JumbleApplication
import com.jumble_x_sale.app.data.model.UserMessageData
import com.jumble_x_sale.app.data.model.UserMessageDataPage
import com.jumble_x_sale.app.data.model.util.Filter
import com.jumble_x_sale.app.data.model.util.FilterPagingOptions
import com.jumble_x_sale.app.util.TimeUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Repository for user messages.
 */
class UserMessageRepository(
    app: JumbleApplication
) : RepositoryBase<UserMessageData, UserMessageDataPage>(app) {

    override fun onRefresh(data: MutableList<UserMessageData>) =
        if (JumbleApplication.isDebugLocal) {
            data.apply {
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 1), message = "Hallo, noch vorhanden?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 2), message = "Moin, letzte Preis?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 3), message = "Griaß di, du grattler gibs ma"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 4), message = "Tschüss"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 5), message = "Hey, funktioniert der noch?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 6), message = "Habe Interesse!"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 7), message = "Kann man handeln?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 8), message = "Wann darf ich ihn abholen?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 9), message = "Ist preislich noch etwas zu machen?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 10), message = "Tauscht du auch?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 11), message = "Macht es Spaß?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 12), message = "Warum verkaufen Sie dies?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 13), message = "Welche Farbe hat es?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 14), message = "In welchem Zustand ist es?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 15), message = "Wie wird es verschickt?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 16), message = "Wie viel kostet der Versand?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 17), message = "Geht 2?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 18), message = "Erweitere bitte die Beschreibung"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 19), message = "Wie viele hast du davon?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 20), message = "Für 20€ nehme ich es!"))
            }
        } else {
            data
        }

    override fun onResetFilters(filters: MutableList<Filter>) { }

    override fun call(filterPagingOptions: FilterPagingOptions): Call<UserMessageDataPage> =
        app.api.userMessageGetByMember(app.userId!!, filterPagingOptions)

    override fun onResult(success: Boolean) {
        if (JumbleApplication.isDebugLocal && success && (data.value?.size ?: 30) < 30) {
            data.value?.apply {
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 21), message = "Hallo ich tausche gegen mein Buch"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 22), message = "Wie wird es Versand?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 23), message = "Kannst du ein paar Bilder machen?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 24), message = "Ist es neu?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 25), message = "Gib mir 10% Rabatt und ich nehme es."))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 26), message = "Bei einem anderen kostet es nur 10€"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 27), message = "Ich wollte das schon immer mal haben, bitte verkauf es mir"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 28), message = "Geht es auch als Einzeilteile zu kaufen?"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 29), message = "Schicke es bitte an 'Hausmacherstraße 13'"))
                add(UserMessageData(creationInstant = TimeUtils.germanInstantOf(2020, 9, 30), message = "Ich nehme es sofort!"))
            }
            data.value = data.value
        }
    }

    companion object {
        @JvmStatic private val TAG = UserMessageRepository::class.simpleName
    }

}
