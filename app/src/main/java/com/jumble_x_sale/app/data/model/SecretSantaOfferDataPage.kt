package com.jumble_x_sale.app.data.model

import com.jumble_x_sale.app.data.model.SecretSantaOfferData
import com.jumble_x_sale.app.data.model.util.PagingOptions
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class SecretSantaOfferDataPage(
    override var options: PagingOptions,
    override var data: List<SecretSantaOfferData>
) : DataPage<SecretSantaOfferData>
