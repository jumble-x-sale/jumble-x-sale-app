@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate", "UNUSED_PARAMETER")

package com.jumble_x_sale.app.data.model.util

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class WindowingOptions(
    // @Transient private var startPosition: Long = 0L,
    // @Transient private var resultCount: Long = 0L
    @Transient private var startPositionField: Long = 0L,
    @Transient private var resultCountField: Long = 0L
    // startPosition: Long = 0L,
    // resultCount: Long = 0L
) : ResultOptions() {

    //@Transient private var startPosition: Long
    //@Transient private var resultCount: Long

    init {
        if (startPositionField < 0L) {
            this.startPositionField = 0L
        }/* else {
            this.startPosition = startPosition
        }*/
        if (resultCountField < 0L) {
            this.resultCountField = 0L
        }/* else {
            this.resultCount = resultCount
        }*/
    }

    // override fun getStartPosition(): Long = startPosition
    // override fun getResultCount(): Long = resultCount

    override val startPosition: Long; get() = startPositionField//; set(value) = field = value
    override val resultCount: Long; get() = resultCountField//; set(value) = field = value

    companion object {
        fun allRows() = WindowingOptions(0L, 0L)
    }
}
