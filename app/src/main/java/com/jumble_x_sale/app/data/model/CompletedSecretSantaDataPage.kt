package com.jumble_x_sale.app.data.model

import com.jumble_x_sale.app.data.model.CompletedSecretSantaData
import com.jumble_x_sale.app.data.model.util.PagingOptions
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class CompletedSecretSantaDataPage(
    override var options: PagingOptions,
    override var data: List<CompletedSecretSantaData>
) : DataPage<CompletedSecretSantaData>
