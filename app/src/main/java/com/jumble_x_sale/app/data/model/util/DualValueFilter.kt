@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.model.util

import com.jumble_x_sale.app.util.parseBigDecimal
import com.jumble_x_sale.app.util.parseInstant
import com.jumble_x_sale.app.util.parseLocalDate
import com.jumble_x_sale.app.util.parseLocalDateTime
import com.jumble_x_sale.app.util.parseLocalTime
import com.jumble_x_sale.app.util.toUuid
import com.jumble_x_sale.app.util.Utils
import com.squareup.moshi.JsonClass
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.Locale
import java.util.UUID
import kotlin.collections.HashMap

@JsonClass(generateAdapter = true)
class DualValueFilter internal constructor(
    propertyName: String?,
    propertyType: DataType?,
    var operator: Operator?,
    var minValue: String?,
    var maxValue: String?
) : Filter(propertyName, propertyType) {

    /**
     * An operator that takes two operands.
     */
    enum class Operator(val code: String) {
        /**
         * Inclusive between. Greater than or equal to the minimum, less than or equal to the
         * maximum.
         */
        BETWEEN(BETWEEN_CODE),
        /**
         * Exclusive between. Greater than or equal to the minimum, less than the maximum.
         */
        IN_BETWEEN(IN_BETWEEN_CODE);

        companion object {
            @JvmStatic private val valuesByCode = HashMap<String, Operator>()

            init {
                val types: Array<Operator> = values()
                for (i in types.indices) {
                    valuesByCode[types[i].code] = types[i]
                }
            }

            fun tryValueOf(idOrCode: String, defaultValue: Operator?): Operator? {
                val idOrCodeUpper = idOrCode.toUpperCase(Locale.ROOT)
                var value = valuesByCode[idOrCodeUpper]
                if (value == null) {
                    value = Utils.tryValueOf(idOrCodeUpper, defaultValue)
                }
                return value
            }

            fun tryValueOf(idOrCode: String): Operator? = tryValueOf(idOrCode, null)
        }
    }

    constructor() : this(null, null, null, null, null)

    fun minValueAsUUID(): UUID? = this.minValue.toUuid()
    fun maxValueAsUUID(): UUID? = this.maxValue.toUuid()
    fun minValueAsDecimal(): BigDecimal? = this.minValue.parseBigDecimal()
    fun maxValueAsDecimal(): BigDecimal? = this.maxValue.parseBigDecimal()
    fun minValueAsInstant(): Instant? = this.minValue.parseInstant()
    fun maxValueAsInstant(): Instant? = this.maxValue.parseInstant()
    fun minValueAsDate(): LocalDate? = this.minValue.parseLocalDate()
    fun maxValueAsDate(): LocalDate? = this.maxValue.parseLocalDate()
    fun minValueAsDateTime(): LocalDateTime? = this.minValue.parseLocalDateTime()
    fun maxValueAsDateTime(): LocalDateTime? = this.maxValue.parseLocalDateTime()
    fun minValueAsTime(): LocalTime? = this.minValue.parseLocalTime()
    fun maxValueAsTime(): LocalTime? = this.maxValue.parseLocalTime()

    companion object {
        private const val BETWEEN_CODE = "BTW"
        private const val IN_BETWEEN_CODE = "IBTW"
        private const val PN_MIN_VALUE = "minValue"
        private const val PN_MAX_VALUE = "maxValue"
    }
}
