package com.jumble_x_sale.app.data.model

import com.squareup.moshi.JsonClass
import java.time.Instant
import java.util.UUID

@JsonClass(generateAdapter = true)
data class UserData(
    var id: UUID? = null,
    var lastLoginDateTime: Instant? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var password: String? = null,
    var email: String? = null,
    var idCard: String? = null
) {

    companion object {
        const val ID = "id"
        const val FIRST_NAME = "firstName"
        const val LAST_NAME = "lastName"
    }

}
