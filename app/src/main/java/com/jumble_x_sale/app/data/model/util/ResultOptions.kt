@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package com.jumble_x_sale.app.data.model.util

///*
//import com.squareup.moshi.JsonClass

//@JsonClass(generateAdapter = true)
abstract class ResultOptions {

    // abstract fun getStartPosition(): Long
    // abstract fun getResultCount(): Long

    abstract val startPosition: Long
    abstract val resultCount: Long

    companion object {
        fun withWindowing(startPosition: Long, resultCount: Long) = WindowingOptions(startPosition, resultCount)
        fun allRows() = WindowingOptions(0L, 0L)
        fun withPaging(pageNumber: Long, pageSize: Long): PagingOptions = PagingOptions(pageNumber, pageSize, 0L)
        fun withPaging() = PagingOptions(0L, 0L, 0L)
    }
}
//*/

/*
interface ResultOptions {

    val startPosition: Long
    val resultCount: Long

    companion object {
        fun withWindowing(startPosition: Long, resultCount: Long) = WindowingOptions(startPosition, resultCount)
        fun allRows() = WindowingOptions(0L, 0L)
        fun withPaging(pageNumber: Long, pageSize: Long): PagingOptions = PagingOptions(pageNumber, pageSize, 0L)
        fun withPaging() = PagingOptions(0L, 0L, 0L)
    }
}
*/
