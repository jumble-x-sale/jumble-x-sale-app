package com.jumble_x_sale.app.data.network

import com.jumble_x_sale.app.core.JumbleApplication
import java.io.IOException
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.Request

class AuthorizationInterceptor(private val app: JumbleApplication) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var mainRequest: Request = chain.request()

        if (app.accessToken != null) {
            mainRequest = mainRequest.setAuth(app)
        }

        return chain.proceed(mainRequest)
    }

    private fun Request.setAuth(app: JumbleApplication): Request {
        return newBuilder()
            .header("Authorization", "Bearer ${app.accessToken}")
            .build()
    }

    companion object {
        private const val AUTH = "Authorization";
    }

}
